//js mutiple
// Choose multiple
$(".sl-multiple").multipleSelect({
  width: "100%",
  single: false,
  filter: true,
  placeholder: "Chọn giá trị",
});

// Choose one
$("#sl-multiple-one")
  .multipleSelect({
    width: "100%",
    single: true,
    filter: true,
    placeholder: "Chọn mã khách hàng",
  })
  .multipleSelect("refresh");

// JS thu gọn mở rộng
var step1;
var step2;
var step3;
var btn1;
var count = 0;

$(document).on("click", ".btn-extend", showAndHide);

function showAndHide() {
  step1 = $(".step.step1");
  step2 = $(".step.step2");
  step3 = $(".step.step3");
  btn1 = $(".btn1");
  count++;
  if (count === 1) {
    step1.removeClass("d-block");
    step2.removeClass("d-none");
    step1.slideUp();
    step3.slideUp();
    step2.slideDown();
  } else if (count === 2) {
    btn1.addClass("d-none");
    step2.slideUp();
    step1.slideUp();
    step3.addClass("d-block rotate");
  } else if (count === 3) {
    btn1.removeClass("d-none");
    step3.removeClass("d-block");
    step1.slideDown();
    count = 0;
  }
}
