
import {CHANGE_LANGUAGE,CHANGE_ENCRYPT_DATA} from "../../helpers/constData"
// khai báo các key để lưu thông tin vào localStorage
const languageStorageKey = "1061CFF7288D449CBF064A2FC55E00E2";
const userStorageKey = "E3946A0E21E04EBA99819C386894B75D";
const dataEncryptStorageKey = "588D896A17DC4D06B70ABCFA53191DFB";
const authenStorageKey="61A15DF733924A4EB35CA268E6343998";

//---------------- Ngon ngu------------------------------------
export const changeLanguage = (dispatch, language) => {
    localStorage.setItem(languageStorageKey, language);
    dispatch({ type: CHANGE_LANGUAGE, payload: language }); 
}

export const getLanguageFromStorage = ()  => {
    return localStorage.getItem(languageStorageKey);
}
export const removeLanguageFromStorage = ()  => {
    return localStorage.removeItem(languageStorageKey);
}
//--------------------------------------------------------------

//----------- Data key được mã hóa từ ipay----------------------
export const saveDataEncryptToStorage = (dispatch, datatoken) => {
    localStorage.setItem(dataEncryptStorageKey, datatoken);
    dispatch({ type: CHANGE_ENCRYPT_DATA, payload: datatoken }); 
}

export const getDataEncryptFromStorage = ()  => {
    return localStorage.getItem(dataEncryptStorageKey);
}

export const removeDataEncryptFromStorage = ()  => {
    return localStorage.removeItem(dataEncryptStorageKey);
}
//---------------------------------------------------------------

// user
export const getUserFromStorage = () => {
    try {
        let _value = localStorage.getItem(userStorageKey) ?? "";
        if(_value === undefined || _value === null) {
            _value = "";
        }
        //
        if(_value !== "") {
            return JSON.parse(_value);
        }
    }
    catch {
    }
    return {};
}

export const saveUserToStorage = (objUser) => {
    localStorage.setItem(userStorageKey, JSON.stringify(objUser));
}

export const removeUserFromStorage = () => {
    localStorage.removeItem(userStorageKey);
}

//-------------------------------------------------------------------

// user
export const getAuthenFromStorage = () => {
    try {
        let _value = localStorage.getItem(authenStorageKey) ?? "";
        if(_value === undefined || _value === null) {
            _value = "";
        }
        //
        if(_value !== "") {
            return JSON.parse(_value);
        }
    }
    catch {
    }
    return {};
}

export const saveAuthenToStorage = (objAuthen) => {
    localStorage.setItem(authenStorageKey, JSON.stringify(objAuthen));
}

export const removeAuthenFromStorage = () => {
    localStorage.removeItem(authenStorageKey);
}