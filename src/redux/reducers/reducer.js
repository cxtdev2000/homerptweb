import { combineReducers } from "redux";
import authReducer from './authReducer';
import { languageReducer } from "./languageReducer";

const reducers = combineReducers({
  auth: authReducer,
  language: languageReducer,
});

export default reducers;
