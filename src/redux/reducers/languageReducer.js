import { getLanguageFromStorage } from "../actions/sharedActions";

import { CHANGE_LANGUAGE } from "../../helpers/constData";

const c_initLanguage = getLanguageFromStorage();

// khởi tạo State
const initState = {
  language: c_initLanguage && c_initLanguage !== "" ? c_initLanguage : "vi",
};

// Reducer thực hiện xử lý làm 1 việc gì đó
// reducer = (state,action) ở đây state = initState và action { type, payload }
export const languageReducer = (state = initState, { type, payload }) => {
  switch (type) {
    case CHANGE_LANGUAGE:
      return {
        ...state,
        language: payload,
      };
    default:
      return state;
  }
};
