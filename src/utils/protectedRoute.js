import React from "react";
import { Route, Navigate, Outlet, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { Layout } from "../components/_Layout";

function ProtectedRoute({}) {
  const user = useSelector((state) => state.auth.user);
  const location = useLocation();

  return user ? (
    <Outlet />
  ) : (
    <Navigate
      to={`/auth/login-page?redirect=${location.pathname ?? "/dashboard/user"}`}
    />
  );
}

export default ProtectedRoute;
