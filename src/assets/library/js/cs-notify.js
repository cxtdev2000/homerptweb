var successClick = function () {
    $.notify({
        // options
        title: '<strong>Thành công</strong>',
        message: "<br>Thành công thêm mới hồ sơ",
        icon: 'glyphicon glyphicon-ok',
        target: '_blank'
    }, {
        // settings
        element: 'body',
        type: "success",
        showProgressbar: false,
        placement: {
            from: "bottom",
            align: "right"
        },  
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
    });
}

var warningClick = function () {
    $.notify({
        // options
        title: '<strong>Cảnh báo</strong>',
        message: "<br>Bạn có muốn hủy hành động này",
        icon: 'glyphicon glyphicon-warning-sign',
    }, {
        // settings
        element: 'body',
        position: null,
        type: "warning",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "bottom",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
    });
}

var dangerClick = function () {
    $.notify({
        // options
        title: '<strong>Lỗi</strong>',
        message: "<br>Không thể lưu hồ sơ mới",
        icon: 'glyphicon glyphicon-remove-sign',
    }, {
        // settings
        element: 'body',
        position: null,
        type: "danger",
        allow_dismiss: true,
        newest_on_top: false,
        showProgressbar: false,
        placement: {
            from: "bottom",
            align: "right"
        },
        offset: 20,
        spacing: 10,
        z_index: 1031,
        delay: 3000999,
        timer: 1000,
        url_target: '_blank',
        mouse_over: null,
        animate: {
            enter: 'animated fadeInRight',
            exit: 'animated fadeOutRight'
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null,
        icon_type: 'class',
    });
}