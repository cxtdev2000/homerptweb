import { toast } from 'react-toastify';

// khai báo các hàm hiển thị thông báo Notify
export const msgSuccess = (name) => {
  toast.dismiss()
  toast.success(name)
};
export const msgError = (name) => {
  toast.dismiss()
  toast.error(name);
}

//chỉnh độ cao của textbox theo số dòng text
export const setHeightInput = (element) => {
  var tx = element;
  tx.setAttribute('style', 'height:' + (tx.scrollHeight) + 'px;overflow-y:hidden;');
}
export const checkDataNotEmpty = (a) => {
  return a && a !== "" && a !== "null";
};
export const commaSeparateNumber = (val) => {
  // remove sign if negative
  var sign = 1;
  if (val < 0) {
    sign = -1;
    val = -val;
  }

  // trim the number decimal point if it exists
  let num = val.toString().includes('.') ? val.toString().split('.')[0] : val.toString();

  while (/(\d+)(\d{3})/.test(num.toString())) {
    // insert comma to 4th last position to the match number
    num = num.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
  }

  // add number after decimal point
  if (val.toString().includes('.')) {
    num = num + '.' + val.toString().split('.')[1];
  }

  // return result with - sign if negative
  return sign < 0 ? '-' + num : num;
}