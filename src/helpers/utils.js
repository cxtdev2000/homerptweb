export const parseJSON = (text) => {
    try {
        if(text && text !== "") {
            return JSON.parse(text);
        }
    } catch (error) {
    }
    return null;
}

export const getLocaleByLanguage = (language) => {
    if(language === 'en') {
        return 'en-GB';
    }
    else if(language === 'vi') {
        return 'vi-VN';
    }
    else {
        return 'en-GB';
    }
}

export const priceToNumber = (strVal) => {
    if(typeof(strVal) === 'number') {
        return strVal;
    }
    return Number((strVal ?? '').replace(/,/g, ''));
} 

export const formatNumber = (numberValue, locale, options) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }
    //
    options = Object.assign({
        minimumFractionDigits: 0, 
        maximumFractionDigits: 2
    }, options);

    if(typeof(numberValue) !== 'number'){
        numberValue = Number(numberValue);
    }

    locale = locale && locale === "" ? 'en' : locale;

    if(numberValue === 0) {
        return '';
    }
    
    return numberValue?.toLocaleString(getLocaleByLanguage(locale), options);
}

export const formatNumber_N1 = (numberValue, locale, options) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }
    //
    options = Object.assign({
        minimumFractionDigits: 0, 
        maximumFractionDigits: 2
    }, options);

    if(typeof(numberValue) !== 'number'){
        numberValue = Number(numberValue);
    }

    locale = locale && locale === "" ? 'en' : locale;
    
    return numberValue?.toLocaleString(getLocaleByLanguage(locale), options);
}

export const formatNumberWithUnit = (numberValue, unit, locale) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }

    if(typeof(numberValue) !== 'number'){
        numberValue = Number(numberValue);
    }

    return formatNumber((numberValue / unit), locale);
}

export const formatNumberWithUnit_N1 = (numberValue, unit, locale) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }

    if(typeof(numberValue) !== 'number'){
        numberValue = Number(numberValue);
    }

    return formatNumber_N1((numberValue / unit), locale);
}

export const formatNumberToM = (numberValue, locale) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }
    return formatNumberWithUnit(numberValue, 1000000, locale);
}

export const formatNumberToM_N1 = (numberValue, locale) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }
    return formatNumberWithUnit_N1(numberValue, 1000000, locale);
}

export const formatNumberToB = (numberValue, locale) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }
    return formatNumberWithUnit(numberValue, 1000000000, locale);
}

export const formatNumberToB_N1 = (numberValue, locale) => {
    if(isNaN(numberValue) === true || numberValue === undefined || numberValue === null) {
        return '';
    }
    return formatNumberWithUnit_N1(numberValue, 1000000000, locale);
}

export const formatPrice = (value, locale, options) => {
    if(isNaN(value) === true || value === undefined || value === null) {
        return '';
    }

    if(typeof(value) !== 'number'){
        value = Number(value);
    }

    return formatNumber((value/ 1000), locale, options);
}

export const formatPrice_N1 = (value, locale, options) => {
    if(isNaN(value) === true || value === undefined || value === null) {
        return '';
    }

    if(typeof(value) !== 'number'){
        value = Number(value);
    }

    return formatNumber_N1((value/ 1000), locale, options);
}

export const formatQtty = (value, locale) => {
    if(isNaN(value) === true || value === undefined || value === null) {
        return '';
    }

    if(typeof(value) !== 'number') {
        value = Number(value);
    }

    const _result = formatNumber(value, locale);
    if(_result && _result.length > 0) {
        return _result.substr(0, _result.length - 1).replace(/([\.,]*)$/g, '');
    }
    else {
        return _result;
    }
}


export const signNumber = (value) => {
    if(priceToNumber(value) > 0) {
        return "+" + value;
    }
    return value;
}

export const roundNumber = (num, scale) => {
    if(!("" + num).includes("e")) {
        return +(Math.round(num + "e+" + scale) + "e-" + scale);
    } else {
        var arr = ("" + num).split("e");
        var sig = ""
        if(+arr[1] + scale > 0) {
            sig = "+";
        }
        return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e-" + scale);
    }
}

export const shortenNumberAuto = (value, language, options) => {
    if(isNaN(value) || value === undefined || value === null) return '';
    //
    options = Object.assign({
        minimumFractionDigits: 0, 
        maximumFractionDigits: 2,
        showMark: true,
    }, options);

    language = language && language === '' ? 'en' : language;
    let _value = 0, _shortenMark = '';

    if(Math.abs(value) >= 1000000000) {
        _value = value / 1000000000;
        _shortenMark = language === 'en' ? 'B' : 'Tỷ';
    }
    else if(Math.abs(value) >= 1000000) {
        _value = value / 1000000;
        _shortenMark = language === 'en' ? 'M' : 'Tr';
    }
    else {
        _value = value;
    }
    //
    return (_value).toLocaleString(getLocaleByLanguage(language), options) + (options.showMark ? _shortenMark : '');
}

export const shortenNumberAutoK = (value, language, options) => {
    if(isNaN(value) || value === undefined || value === null) return '';
    //
    options = Object.assign({
        minimumFractionDigits: 0, 
        maximumFractionDigits: 2,
        showMark: true,
    }, options);

    language = language && language === '' ? 'en' : language;
    let _value = 0, _shortenMark = '';

    if(Math.abs(value) >= 1000000) {
        return shortenNumberAuto(value, language, options);
    }
    else if(Math.abs(value) >= 1000) {
        _value = value / 1000;
        _shortenMark = language === 'en' ? 'K' : 'N';
    }
    else {
        _value = value;
    }
    //
    return (_value).toLocaleString(getLocaleByLanguage(language), options) + (options.showMark ? _shortenMark : '');
}

export const getShortenMark = (value, language) => {
    if(isNaN(value) || value === undefined || value === null) return '';

    let _shortenMark = '';

    if(Math.abs(value) >= 1000000000) {
        _shortenMark = language === 'en' ? 'B' : 'Tỷ';
    }
    else if(Math.abs(value) >= 1000000) {
        _shortenMark = language === 'en' ? 'M' : 'Tr';
    }
    else if(Math.abs(value) >= 1000) {
        _shortenMark = language === 'en' ? 'K' : 'N';
    }
   
    return _shortenMark;
}

export const getColorByPrice = (price, ceilPrice, floorPrice, refPrice) => {
    if(price === refPrice)  {
        return 'clr-ref';
    }
    else if(price >= ceilPrice)
    {
        return 'clr-ceil';
    }
    else if(price <= floorPrice) 
    {
        return 'clr-floor';
    }
    else if(price < refPrice) 
    {
        return 'clr-down';
    }
    else if(price > refPrice) 
    {
        return 'clr-up';
    }
    else {
        return "";
    }
}


export const getColorByIndex = (value, priorValue) => {
    if(value === priorValue)  {
        return 'clr-ref';
    }
    else if(value < priorValue) 
    {
        return 'clr-down';
    }
    else if(value > priorValue) 
    {
        return 'clr-up';
    }
    else {
        return "";
    }
}

export const camelCaseObject = (o) => {
    var newO, origKey, newKey, value;
    if (o instanceof Array) {
        return o.map(function (value) {
            if (typeof value === "object") {
                value = camelCaseObject(value);
            }
            return value;
        });
    } else {
        newO = {};
        for (origKey in o) {
            if (o.hasOwnProperty(origKey)) {
                newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString();
                value = o[origKey];
                if (value instanceof Array || (value !== null && value.constructor === Object)) {
                    value = camelCaseObject(value);
                }
                newO[newKey] = value;
            }
        }
    }
    return newO;
}

//--Đọc số------------------------------------------------------------------------------------------------------------------------------------------
let mangso = ['không', 'một', 'hai', 'ba', 'bốn', 'năm', 'sáu', 'bảy', 'tám', 'chín'];
function dochangchuc(so, daydu) {
    let chuoi = "";
    let chuc = Math.floor(so / 10);
    let donvi = so % 10;
    if (chuc > 1) {
        chuoi = " " + mangso[chuc] + " mươi";
        if (donvi === 1) {
            chuoi += " mốt";
        }
    } else if (chuc === 1) {
        chuoi = " mười";
        if (donvi === 1) {
            chuoi += " một";
        }
    } else if (daydu && donvi > 0) {
        chuoi = " lẻ";
    }
    if (donvi === 5 && chuc >= 1) {
        chuoi += " lăm";
    } else if (donvi > 1 || (donvi === 1 && chuc === 0)) {
        chuoi += " " + mangso[donvi];
    }
    return chuoi;
}
function docblock(so, daydu) {
    let chuoi = "";
    let tram = Math.floor(so / 100);
    so = so % 100;
    if (daydu || tram > 0) {
        chuoi = " " + mangso[tram] + " trăm";
        chuoi += dochangchuc(so, true);
    } else {
        chuoi = dochangchuc(so, false);
    }
    return chuoi;
}
function dochangtrieu(so, daydu) {
    let chuoi = "";
    let trieu = Math.floor(so / 1000000);
    so = so % 1000000;
    if (trieu > 0) {
        chuoi = docblock(trieu, daydu) + " triệu";
        daydu = true;
    }
    let nghin = Math.floor(so / 1000);
    so = so % 1000;
    if (nghin > 0) {
        chuoi += docblock(nghin, daydu) + " nghìn";
        daydu = true;
    }
    if (so > 0) {
        chuoi += docblock(so, daydu);
    }
    return chuoi;
}
function DocSoDuong(so) {
    if (so === 0) return mangso[0];
    let chuoi = "", hauto = "";
    do {
        let ty = so % 1000000000;
        so = Math.floor(so / 1000000000);
        if (so > 0) {
            chuoi = dochangtrieu(ty, true) + hauto + chuoi;
        } else {
            chuoi = dochangtrieu(ty, false) + hauto + chuoi;
        }
        hauto = " tỷ";
    } while (so > 0);
    let strReturn = chuoi.trim().charAt(0).toUpperCase() + chuoi.trim().slice(1);
    return strReturn;
}

function DocSoAm(so) {
    let checkSoAm = false;
    if (so.toString().charAt(0) === "-") {
        so = so.toString().replace('-', '');
        checkSoAm = true;
    }

    if (so === 0) return mangso[0];
    let chuoi = "", hauto = "";
    do {
        let ty = so % 1000000000;
        so = Math.floor(so / 1000000000);
        if (so > 0) {
            chuoi = dochangtrieu(ty, true) + hauto + chuoi;
        } else {
            chuoi = dochangtrieu(ty, false) + hauto + chuoi;
        }
        hauto = " tỷ";
    } while (so > 0);
    var strReturn = chuoi.trim().toString().charAt(0).toUpperCase() + chuoi.trim().slice(1);
    if (checkSoAm === true) {
        strReturn = "Âm " + chuoi.trim();
    }
    return strReturn;
}

export function ReadNumber(so){
    if (so.toString().charAt(0) === "-") {
        return DocSoAm(so);
    }
    else {
        return DocSoDuong(so);
    }
}
//----Kết thúc đọc số----------------------------------------------------------------------------------------------------------------------------------------

export function IsValidEmail(email) {
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
    if (reg.test(email)) {
        return true;
    }
    else {
        return false;
    }
  }