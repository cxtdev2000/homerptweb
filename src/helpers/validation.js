export const checkValidationUser = (step, field) => {
  let errorMsg = "";
  // console.log('thong tin cac truong lay duoc',field);
  //
  if (step === 1) {
    if (!field.loaiKhachHang) {
      errorMsg = "Vui lòng chọn loại khách hàng.";
      //debugger;
    }
    // if (IsContainUnicode(field.email) ){
    // }
    // else if (!field.email) {
    //   errorMsg = "Vui lòng nhập Email.";
    // }
    // else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(field.email)) {
    //   errorMsg = "Email không hợp lệ";
    // }
  }
  return errorMsg;
};



function IsContainUnicode(pStrString) {
  try {
    let strRegex =
      /à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|ì|í|ị|ỉ|ĩ|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|ỳ|ý|ỵ|ỷ|ỹ|đ/gi;
    return strRegex.test(pStrString);
  } catch (e) {
    return false;
  }
}

function IsValidEmail(email) {
  var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
  if (reg.test(email)) {
      return true;
  }
  else {
      return false;
  }
}

function IsValidOnlyNumber(cmt) {
  try {
      cmt = cmt || "";
      cmt = cmt.replace(/ /g, '');
      return /[0-9]$/.test(cmt);
  } catch (e) {
      console.log(e.message);
      return false;
  }
}
