import { Button, Empty } from "antd";
import React from "react";

// ant table locale
export const locale = {
  // nếu không có bản ghi nào hiển thị:
  emptyText: <Empty description={<span>Không có dữ liệu</span>}></Empty>,
  triggerDesc: "Sắp xếp Z -> A",
  triggerAsc: "Sắp xếp A -> Z",
  cancelSort: "Không sắp xếp",
};
