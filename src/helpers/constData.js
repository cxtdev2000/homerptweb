import { t as trans } from "../translations/T";
import locale from "antd/es/locale/vi_VN";
import { Button, Empty } from "antd";
import moment from "moment";

const env = process.env.NODE_ENV || "development";
const logenv = console.log("logenv:", process.env);
const port = window.location.port;
const host = window.location.hostname;

export const AUTH_SERVER_URL =
  host == "103.176.179.201"
    ? "http://103.176.179.201:5000"
    : "http://localhost:5000";
// : "http://103.176.179.201:5000/api";

const apiUrl = AUTH_SERVER_URL;

export const constHostAddressConfig = {
  ApiHostAddress: apiUrl,
};

// khai báo các hằng số
export const CHANGE_LANGUAGE = "CHANGE_LANGUAGE";
export const SET_USER = "SET_USER";
export const CLEAR_USER = "CLEAR_USER";
export const CHANGE_ENCRYPT_DATA = "CHANGE_ENCRYPT_DATA";
export const SET_AUTHEN = "SET_AUTHEN";
export const CLEAR_AUTHEN = "CLEAR_AUTHEN";

export const useGlobalConst = (t = trans) => {
  return {
    APP: {
      NAME: "BOClient",
      FORM: {
        TYPE: {
          TEXTBOX: "",
          COMBOBOX_MULTI: "CBM",
          COMBOBOX_SINGLE: "CBS",
        },
      },
      CODE: {
        STATUS: {
          PENDING: "N",
          APPROVED: "A",
          REJECTED: "R",
          EXPIRED: "E",
        },
      },
    },
    ANT: {
      LOCALE: {
        ...locale,
        lang: {
          ...locale.lang,
        },
        timePickerLocale: {
          ...locale.timePickerLocale,
        },
        dateFormat: "DD/MM/YYYY",
        dateTimeFormat: "DD/MM/YYYY HH:mm:ss",
        weekFormat: "YYYY-wo",
        monthFormat: "YYYY-MM",
        emptyText: <Empty description={<span>{t("koCoDuLieu")}</span>}></Empty>,
      },
      FORM: {
        RULES: {
          yeuCauNhap: {
            required: true,
            message: `\${label} ${t("koDcDeTrong")}`,
          },
          email: {
            type: "email",
            message: `\${label} không đúng định dạng email`,
          },
          khongKhoangTrang: {
            pattern: /[^\s]/, // không chỉ có khoảng trắng
            message: `\${label} ${t("không hợp lệ")}`,
          },
          khongCoBatKyKhoangTrangNao: ({ getFieldValue }) => ({
            validator(_, value) {
              if (value?.trim().indexOf(" ") >= 0) {
                return Promise.reject(
                  new Error(`\${label} ${t("koDcCoKhoangTrang")}`),
                );
              }
              return Promise.resolve();
            },
          }),
          gioiHan20KiTu: {
            pattern: /^[\w\W]{1,20}$/,
            message: `\${label} ${t("gioiHan20KyTu")}`,
          },
          chiDuoc20KiTu: ({ getFieldValue }) => ({
            validator(_, value) {
              if (value?.trim().length != 20) {
                return Promise.reject(
                  new Error(`\${label} ${t("chiDc20KyTu")}`),
                );
              }
              return Promise.resolve();
            },
          }),
          gioiHan200KiTu: {
            pattern: /^[\w\W]{1,200}$/,
            message: `\${label} ${t("gioiHan200KyTu")}`,
          },
          gioiHan500KiTu: {
            pattern: /^[\w\W]{1,500}$/,
            message: `\${label} ${t("gioiHan500KyTu")}`,
          },
          gioiHan10KiTu: {
            pattern: /^[\w\W]{1,10}$/,
            message: `\${label} ${t("gioiHan10KyTu")}`,
          },
          gioiHan30KiTu: {
            pattern: /^[\w\W]{1,30}$/,
            message: `\${label} ${t("gioiHan30KyTu")}`,
          },
          chiDuoc10KiTu: {
            pattern: /^[\w\W]{10,10}$/,
            message: `\${label} ${t("phaiLa10KyTu")}`,
          },
          chiNhapSo: {
            pattern: /^\d+$/,
            message: `\${label} ${t("phaiLaSo")}`,
          },
        },
        ITEM: {
          PARSER: {
            DATE_MOMENT: {
              // parse sang moment js, dùng cho DatePicker
              getValueProps: (i) => ({ value: i ? moment(i) : i }),
            },
            TEXT_TRIMALL: {
              //trim cả chuỗi
              getValueProps: (i) => ({
                value: typeof i == "string" ? i?.trim() : i,
              }),
            },
            TEXT_TRIMEND: {
              //chỉ trim cuối chuỗi
              getValueProps: (i) => ({
                value: typeof i == "string" ? i?.trimEnd() : i,
              }),
            },
            TEXT_TRIMSTART: {
              //chỉ trim đầu chuỗi
              getValueProps: (i) => ({
                value: typeof i == "string" ? i?.trimStart() : i,
              }),
            },
          },
        },
      },
    },
    MODULE: {
      SA: {
        STATUS: {
          PENDING: "N",
          APPROVED: "A",
          REJECTED: "R",
          PENDINGUPDATE: "U",
          PENDINGDELETE: "D",
          EXPIRED: "E",
        },
        STATUS_SET: [
          {
            code: "N",
            badge: "processing",
            iconClass: ".primary",
          },
          {
            code: "U",
            badge: "warning",
            iconClass: ".primary",
          },
          {
            code: "D",
            badge: "warning",
            iconClass: ".primary",
          },
          {
            code: "A",
            badge: "success",
            iconClass: ".primary",
          },
          {
            code: "R",
            badge: "error",
            iconClass: ".primary",
          },
          {
            code: "E",
            badge: "error",
            iconClass: ".primary",
          },
        ],
        ConstUsers: Object.freeze({
          CATEGORY_VN: t("nguoiDung"),
          CONTROLLER: "User",
          FORM: {
            LAYOUT: {
              FULLSCREEN: true,
            },
            ADD: {
              INITIAL: { active: true, role: "giam-sat" },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              code: "USER_LIST",
              title: "Danh sách người dùng",
              titleEn: "User list",
              keyWord: "1001",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "CODE",
                  title: "Mã người dùng",
                  titleEn: "User code",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "FULLNAME",
                  title: "Tên",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "Id_Ward",
                  title: "Phường, xã",
                  titleEn: "Ward",
                  dataType: "",
                  control: "CBS",
                  defValues: [],
                  operators: ["="],
                  options: [
                    { cdVal: "N", content: "Miền Bắc", contentEn: "North" },
                    { cdVal: "S", content: "Miền Nam", contentEn: "South" },
                    { cdVal: "W", content: "Miền Tây", contentEn: "West" },
                    { cdVal: "C", content: "Miền Trung", contentEn: "Center" },
                  ],
                },
                {
                  key: "ADDRESS",
                  title: "Địa chỉ",
                  titleEn: "Address",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
              ],
              columnList: [
                // {
                //   key: "active",
                //   title: "Kích hoạt",
                //   titleEn: "No",
                //   dataType: "SWITCH",
                //   align: "C",
                //   fixed: "L",
                //   position: 1,
                //   width: 80,
                //   sortable: "Y",
                // },
                {
                  key: "code",
                  title: "Mã người dùng",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "fullName",
                  title: "Tên người dùng",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                // {
                //   key: "group",
                //   title: "Nhóm người dùng",
                //   titleEn: "Depart code",
                //   dataType: "",
                //   align: "",
                //   fixed: "L",
                //   position: 2,
                //   width: 140,
                //   sortable: "Y",
                // },
                {
                  key: "roleName",
                  title: "Chức quyền",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 160,
                  sortable: "Y",
                },
                {
                  key: "email",
                  title: "Email",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 160,
                  sortable: "Y",
                },
                {
                  key: "phoneNumber",
                  title: "Số điện thoại",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 160,
                  sortable: "Y",
                },
                {
                  key: "birthdate",
                  title: "Ngày sinh",
                  titleEn: "Address",
                  dataType: "DAT10",
                  align: "",
                  fixed: "",
                  position: 9,
                  width: 120,
                  sortable: "Y",
                },
                // {
                //   key: "description",
                //   title: "Mô tả",
                //   titleEn: "Description",
                //   dataType: "",
                //   align: "",
                //   fixed: "",
                //   position: 12,
                //   width: 120,
                //   sortable: "Y",
                // },
                {
                  key: "userName",
                  title: "Tài khoản",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 160,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["id"],
            },
          },
        }),
        ConstProvinces: Object.freeze({
          CATEGORY_VN: "Tỉnh/thành phố",
          CONTROLLER: "Province",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              INITIAL: { active: true },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              // code: "_LIST",
              title: "Danh sách tỉnh/thành phố",
              titleEn: "User list",
              keyWord: "1101",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "code",
                  title: "Mã tỉnh/tp",
                  titleEn: "User code",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "fullName",
                  title: "Tên tỉnh/tp",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "zipCode",
                  title: "Zipcode",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                // {
                //   key: "zone_id",
                //   title: "Vùng miền",
                //   titleEn: "Ward",
                //   dataType: "",
                //   control: "CBS",
                //   defValues: [],
                //   operators: ["="],
                //   options: [
                //     { cdVal: "N", content: "Miền Bắc", contentEn: "North" },
                //     { cdVal: "S", content: "Miền Nam", contentEn: "South" },
                //     { cdVal: "W", content: "Miền Tây", contentEn: "West" },
                //     { cdVal: "C", content: "Miền Trung", contentEn: "Center" },
                //   ],
                // },
              ],
              columnList: [
                // {
                //   key: "active",
                //   title: "Kích hoạt",
                //   titleEn: "No",
                //   dataType: "SWITCH",
                //   align: "C",
                //   fixed: "L",
                //   position: 1,
                //   width: 80,
                //   sortable: "Y",
                // },
                {
                  key: "code",
                  title: "Mã tỉnh/tp",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "administrativeRegion",
                  title: "Vùng/miền",
                  titleEn: "Depart code",
                  dataType: "obj",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "fullName",
                  title: "Tên tỉnh/tp",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "zipCode",
                  title: "Zip code",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 160,
                  sortable: "Y",
                },
                {
                  key: "description",
                  title: "Mô tả",
                  titleEn: "Description",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 12,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
        ConstDistricts: Object.freeze({
          CATEGORY_VN: "Quận/huyện",
          CONTROLLER: "District",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              INITIAL: { active: true },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              // code: "_LIST",
              title: "Danh sách quận/huyện",
              titleEn: "User list",
              keyWord: "1102",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "code",
                  title: "Mã quận/huyện",
                  titleEn: "User code",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "fullName",
                  title: "Tên quận/huyện",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
              ],
              columnList: [
                {
                  key: "code",
                  title: "Mã quận/huyện",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "provinceCodeNavigation",
                  title: "Tỉnh/thành phố",
                  titleEn: "Depart code",
                  dataType: "obj",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "fullName",
                  title: "Tên quận/huyện",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
        ConstWards: Object.freeze({
          CATEGORY_VN: "Phường/xã",
          CONTROLLER: "Ward",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              INITIAL: { active: true },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              // code: "_LIST",
              title: "Danh sách phường/xã",
              titleEn: "User list",
              keyWord: "1102",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "code",
                  title: "Mã phường/xã",
                  titleEn: "User code",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "fullName",
                  title: "Tên phường/xã",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
              ],
              columnList: [
                {
                  key: "code",
                  title: "Mã phường/xã",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "districtCodeNavigation",
                  title: "Quận/huyện",
                  titleEn: "Depart code",
                  dataType: "obj",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "fullName",
                  title: "Tên phường/xã",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
        ConstAdministrativeRegions: Object.freeze({
          CATEGORY_VN: "Vùng/miền",
          CONTROLLER: "AdministrativeRegion",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              INITIAL: { active: true },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              // code: "_LIST",
              title: "Danh sách Vùng/miền",
              titleEn: "User list",
              keyWord: "1102",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "name",
                  title: "Tên Vùng/miền",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
              ],
              columnList: [
                {
                  key: "id",
                  title: "Mã Vùng/miền",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },

                {
                  key: "name",
                  title: "Tên Vùng/miền",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
        ConstRoles: Object.freeze({
          CATEGORY_VN: "Chức quyền",
          CONTROLLER: "Role",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              DISABLED: true,
              INITIAL: { active: true },
            },
            EDIT: {
              DISABLED: true,
              INITIAL: {
                // actsive: 1,
              },
            },
            DETAIL: {
              DISABLED: true,
            },
            DELETE: {
              DISABLED: true,
            },
          },
          SEARCH: {
            DISABLED: true,
            INQUIRY: {
              // code: "_LIST",
              title: "Danh sách Chức quyền",
              titleEn: "User list",
              keyWord: "1102",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "name",
                  title: "Tên Chức quyền",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
              ],
              columnList: [
                {
                  key: "name",
                  title: "Tên Chức quyền",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "description",
                  title: "Mô tả",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
        ConstGroups: Object.freeze({
          CATEGORY_VN: "Nhóm người sử dụng",
          CONTROLLER: "Group",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              INITIAL: { active: true },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              // code: "_LIST",
              title: "Danh sách nhóm người sử dụng",
              titleEn: "User list",
              keyWord: "1102",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "name",
                  title: "Tên nhóm người sử dụng",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
              ],
              columnList: [
                {
                  key: "id",
                  title: "Mã nhóm người sử dụng",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "name",
                  title: "Tên nhóm người sử dụng",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "description",
                  title: "Mô tả",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
      },
      DE: {
        ConstDevices: Object.freeze({
          CATEGORY_VN: t("thietBi"),
          CONTROLLER: "Device",
          FORM: {
            LAYOUT: {
              FULLSCREEN: false,
            },
            ADD: {
              INITIAL: { active: true, role: "giam-sat" },
            },
            EDIT: {
              INITIAL: {
                // actsive: 1,
              },
            },
          },
          SEARCH: {
            INQUIRY: {
              code: "DEVICE_LIST",
              title: "Danh sách thiết bị",
              titleEn: "User list",
              keyWord: "1001",
              exportType: "",
              table: "",
              filterList: [
                {
                  key: "CODE",
                  title: "Mã thiết bị",
                  titleEn: "User code",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "NAME",
                  title: "Tên thiết bị",
                  titleEn: "Name",
                  dataType: "",
                  control: "",
                  defValues: [],
                  operators: ["LIKE"],
                  options: [],
                },
                {
                  key: "Trạng thái",
                  title: "Trạng thái",
                  titleEn: "",
                  dataType: "",
                  control: "CBS",
                  defValues: [],
                  operators: ["="],
                  options: [
                    { cdVal: "A", content: "Hoạt động", contentEn: "" },
                    { cdVal: "D", content: "Không hoạt động", contentEn: "" },
                  ],
                },
              ],
              columnList: [
                {
                  key: "code",
                  title: "Mã thiết bị",
                  titleEn: "Depart code",
                  dataType: "",
                  align: "",
                  fixed: "L",
                  position: 2,
                  width: 140,
                  sortable: "Y",
                },
                {
                  key: "name",
                  title: "Tên thiết bị",
                  titleEn: "Depart name",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 4,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "group",
                  title: "Nhóm thiết bị",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "address",
                  title: "Địa chỉ lắp đặt",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 300,
                  sortable: "Y",
                },
                {
                  key: "location",
                  title: "Toạ độ",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "powerType",
                  title: "Loại nguồn điện",
                  titleEn: "Address",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 9,
                  width: 200,
                  sortable: "Y",
                },
                {
                  key: "staff",
                  title: "Nhân viên quản lý",
                  titleEn: "Depart type",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 6,
                  width: 160,
                  sortable: "Y",
                },
                {
                  key: "createDate",
                  title: "Ngày tạo",
                  titleEn: "Create date",
                  dataType: "DAT16",
                  align: "",
                  fixed: "",
                  position: 22,
                  width: 120,
                  sortable: "Y",
                },
                {
                  key: "createBy",
                  title: "Người tạo",
                  titleEn: "User",
                  dataType: "",
                  align: "",
                  fixed: "",
                  position: 23,
                  width: 100,
                  sortable: "Y",
                },
              ],
              recordKeys: ["code"],
            },
          },
        }),
      },
    },
  };
};
export default useGlobalConst;
