import axios from "axios";
import { constHostAddressConfig } from "./constData";
import { getDataEncryptFromStorage } from "../redux/actions/sharedActions";
import { useGlobalConst } from "./constData";
import { t } from "i18next";
import { useSelector } from "react-redux";

const c_DataEncrypt = getDataEncryptFromStorage();

console.log(localStorage.getItem("userLocation"));

const axiosClient = axios.create({
  baseURL: constHostAddressConfig.ApiHostAddress,
  //   headers: {
  //     "Content-Type": "application/json",
  //     "Access-Control-Allow-Origin": "*",
  //     "Access-Control-Request-Private-Network": true,
  //     "Access-Control-Allow-Private-Network": true,
  //     "Access-Control-Allow-Headers":
  //       "Origin, Content-Type, Accept, Authorization, X-Request-With",
  //     "Access-Control-Allow-Methods": "GET,HEAD,OPTIONS,POST,PUT,DELETE",
  //   },
});
export const useAxios = () => {
  const globalConst = useGlobalConst(t);
  const lang = useSelector((state) => state.language.language);

  return {
    get: (url) => axiosClient.get(url),
    post: (url, payload, config) =>
      axiosClient.post(
        url,
        {
          ...payload,
          appName: globalConst.APP.NAME,
          ClientLanguage: lang,
        
          // wsName: "macbook",
          actionTime: new Date(),
        },
        config,
      ),
  };
};

//get ip address
export const getIpAddress = () => {
  return axiosClient.get("https://geolocation-db.com/json/");
};

//get allcode
export const getAllCode = () => {
  return axiosClient.get("api/Allcode/getall");
};

//get allcode by cdtype cdname
export const getAllCodeByCdtypeCdName = (cdType, cdName) => {
  return axiosClient.get(
    `api/Allcode/getbycdtypecdcode?cdType=${cdType}&cdCode=${cdName}`,
  );
};
//search get by search code
export const getSearchByCode = (cdSearch) => {
  return axiosClient.get(`api/Inquiry/getbycode?code=${cdSearch}`);
};

export const getGroups = () => {
  return axiosClient.get(`api/Group/`);
};
//#region Khu vực Việt Nam
export const getRegions = () => {
  return axiosClient.get(`api/AdministrativeRegion/`);
};
export const getProvinces = () => {
  return axiosClient.get(`api/Province/`);
};
export const getDistricts = () => {
  return axiosClient.get(`api/District/`);
};
export const getWards = () => {
  return axiosClient.get(`api/Ward/`);
};
//#endregion
