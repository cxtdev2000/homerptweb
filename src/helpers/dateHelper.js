
//format thành yyyy-mm-dd
export const formatDate = (date) => {
  if (!date || date=="1-01-01") return "";

  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
};
//cộng năm
export const plusYear = (date, year) => {
  date = new Date(date);
  date.setFullYear(date.getFullYear() + year);
  return date;
};

export const getDate = (d) => {
  return d && d != "0001-01-01T00:00:00"? new Date(d)  : ""
}
