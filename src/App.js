// Author : Can Xuan Tung – Navisoft.
import { ConfigProvider } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
//import PageNotFound from "./components/PageNotFound";
import { Layout } from "./components/_Layout";
import useScript from "./hooks/useScript";

import { Provider } from "react-redux";

import { List as User } from "./components/systemParams-component/user/components/List";
import { List as Group } from "./components/systemParams-component/group/components/List";
import { List as Role } from "./components/systemParams-component/role/components/List";

import { List as Province } from "./components/systemParams-component/province/components/List";
import { List as District } from "./components/systemParams-component/district/components/List";
import { List as Ward } from "./components/systemParams-component/ward/components/List";
import { List as Region } from "./components/systemParams-component/region/components/List";

import { List as Device } from "./components/systemParams-component/device/components/List";

import { LoginPage } from "./components/account-component/LoginPage";
import ProtectedRoute from "./utils/protectedRoute";
import store from "./redux/store";
import oauth, { userProfile } from "./services/oauth";
import AuthProvider from "./utils/authProvider";

import { library } from "@fortawesome/fontawesome-svg-core";
import { default as localeEN } from "antd/es/locale/en_US";
import { default as localeVN } from "antd/es/locale/vi_VN";
import * as apiHelper from "./helpers/apiHelper";

import {
  faCheckSquare,
  faChevronDown,
  faChevronUp,
  faCircleQuestion,
  faCoffee,
  faFileCsv,
  faFileExcel,
  faFilePdf,
  faFileWord,
} from "@fortawesome/free-solid-svg-icons";
// api check thông tin khách hàng
//
//css
import "antd/dist/antd.css";
import "./assets/library/css/all.css";
import "./assets/library/css/animate.css";
import "./assets/library/css/bootstrap.min.css";
import "./assets/library/css/cs-ant.css";
import "./assets/library/css/jquery.datetimepicker.min.css";
import "./assets/library/css/multiple-select.css";
import "./assets/library/css/swiper.css";
import "./assets/scss/main.css";
import "./index.css";

//custom lib css
import "./App.css";
import { DashBoard } from "./components/GiamSat/GiamSatHeThong/DashBoard";

//font-awesome global
library.add(
  faCheckSquare,
  faCoffee,
  faChevronUp,
  faChevronDown,
  faCircleQuestion,
  faFileWord,
  faFileExcel,
  faFilePdf,
  faFileCsv,
);

//import ListOrderSell from "./components/order-component/ListOrderSell";

// CHÚ Ý
// encodeURIComponent mã hóa nhiều ký tự url không hiểu
// encodeURIComponent('strEncrypt')`
// khi từ IB gửi sang phải encode url để trình duyệt hiểu chuỗi
// bở vì những dấu /,+,. => trình duyệt đang không đọc được
// ví dụ: encodeURIComponent('pCpC4M/4iVWt8eXjePdgygHNOktbOUpVc9nMTkheRARL8sxw97bIKv0Lw7cu3CJWmQzYrXNI5ls0nUU3wCn9OyW33daAyu8e6/Q7HBnO+gBHmE8lwZWS36fT2Yqph2m1huPry7DoHA7uy2GhIGiiRA==')
// sau khi mã hóa theo encodeURIComponent thì được chuỗi: pCpC4M%2F4iVWt8eXjePdgygHNOktbOUpVc9nMTkheRARL8sxw97bIKv0Lw7cu3CJWmQzYrXNI5ls0nUU3wCn9OyW33daAyu8e6%2FQ7HBnO%2BgBHmE8lwZWS36fT2Yqph2m1huPry7DoHA7uy2GhIGiiRA%3D%3D

function App() {
  const dispatch = useDispatch();
  const language = useSelector((state) => state.language.language);
  const [locale, setLocale] = useState(language == "en" ? localeEN : localeVN);

  useEffect(() => {
    setLocale(language == "en" ? localeEN : localeVN);
    return () => {};
  }, [language]);

  useScript("./library/js/jquery.min.js");
  useScript("./library/js/bootstrap.min.js");
  useScript("./library/js/swiper.js");
  useScript("./library/js/jquery.datetimepicker.full.min.js");
  useScript("./library/js/wow.min.js");
  useScript("./library/js/jquery.multiple.select.js");
  useScript("./library/js/script.js");

  useEffect(() => {
    // async function fetchAllCode(disType) {
    //   const { data: loaded } = await apiHelper.getAllCode();
    //   console.log(loaded);
    //   dispatch({ type: disType, payload: loaded });
    // }
    // async function getIpAddress() {
    //   const { data: loaded } = await apiHelper.getIpAddress();
    //   localStorage.setItem("userLocation", JSON.stringify(loaded));
    // }

    // getIpAddress(); // lấy location người dùng
    // fetchAllCode("SET_ALLCODE");
    async function fetchUserInfo() {
      const { data: loaded } = await userProfile();
      console.log(loaded);
      dispatch({ type: "STORE_USER", payload: loaded });
    }

    fetchUserInfo();
  }, []);
  return (
    <ConfigProvider locale={locale}>
      <Provider store={store}>
        <AuthProvider userManager={oauth} store={store}>
          <BrowserRouter>
            <Layout>
              <Routes>
                {/* Xác thực route  */}
                <Route path="/dashboard" element={<ProtectedRoute />}>
                  <Route index element={<DashBoard />} />

                  #region người sử dụng
                  <Route path="user" element={<User />} />
                  <Route path="group" element={<Group />} />
                  <Route path="role" element={<Role />} />

                  #region vùng địa lý
                  <Route path="zone" element={<Region />} />
                  <Route path="province" element={<Province />} />
                  <Route path="district" element={<District />} />
                  <Route path="ward" element={<Ward />} />

                  #region #region thiết bị
                  <Route path="device" element={<Device />} />
                </Route>
                <Route exact path="/auth/login-page" element={<LoginPage />} />
                {/* <Route exact path="/auth" element={AuthLayout} /> */}
              </Routes>
            </Layout>
          </BrowserRouter>
        </AuthProvider>
      </Provider>
    </ConfigProvider>
  );
}

export default App;
