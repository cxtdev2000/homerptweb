import React from "react";

function LoadingSpinner() {
  return (
    <div className="spin-wrap">
      <div className="spinner-container">
        <div className="loading-spinner"></div>
      </div>
    </div>
  );
}

export default LoadingSpinner;
