
import React,{useState} from 'react';

function ToolEncodeUrl(props) {
    const [dataText,setDataText]=useState("");

    const handleClick = (e) => {
        setDataText(encodeURIComponent(e));
    }
    return (
        <div>
            Nhập vào: <input type="text" style={{width:'100%'}} onChange={(e)=>handleClick(e.target.value)} />            <br/>
            Kq: <input type="text" style={{width:'100%'}} value={`?data=${dataText}`} />            <br/>
        </div>
    );
}

export default ToolEncodeUrl;