import { useTranslation } from "react-i18next";
import React, { useEffect, useState, useRef } from "react";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import {
  Switch,
  Button,
  Popover,
  BackTop,
  Calendar,
  Col,
  Radio,
  Row,
  Select,
  Typography,
} from "antd";
import { hanldeChangeLanguage } from "../translations/T";
import { useDispatch, useSelector } from "react-redux";
import { text } from "@fortawesome/fontawesome-svg-core";
import { t } from "../translations/T";

const style = {
  height: 40,
  width: 40,
  right: "20px",
  lineHeight: "40px",
  borderRadius: 4,
  color: "#fff",
  textAlign: "center",
  fontSize: 14,
};

export const Footer = (props) => {
  const dispatch = useDispatch();
  const language = useSelector((state) => state.language.language);

  const [dateString, setDateString] = useState(
    new Date().toLocaleDateString("en-US"),
  );
  const [timeString, setTimeString] = useState(
    new Date().toTimeString().split(" ")[0],
  );

  const handleSwitchLanguage = (checked, event) => {
    if (checked) hanldeChangeLanguage("en", dispatch);
    else hanldeChangeLanguage("vi", dispatch);
  };

  useEffect(() => {
    setInterval(() => {
      setDateString(new Date().toLocaleDateString("en-US"));
      setTimeString(new Date().toTimeString().split(" ")[0]);
    }, 1000);
    return () => {};
  }, []);

  return (
    <>
      <div className="footer">
        <div className="d-flex justify-content-between">
          <div className="session">
            <img src="/image/icon/ic-session.svg" alt="" />
            <div>{t("trongPhien")}</div>
          </div>
          <div className="times">
            <div className="mr-2">
              <Switch
                checkedChildren="EN"
                unCheckedChildren="VI"
                defaultChecked={language == "en"}
                onChange={handleSwitchLanguage}
              />
            </div>
            <img src="/image/icon/ic-calendar-clock.svg" alt="" />
            <Popover
              placement="topRight"
              content={
                <div
                  style={{
                    width: "302px",
                    border: "1px solid #f0f0f0",
                    borderRadius: "2px",
                  }}
                >
                  <Calendar fullscreen={false} />
                </div>
              }
              title="Lịch"
              trigger="hover"
            >
              <div id="dateDisplay">{dateString}</div>
            </Popover>
            -<div id="timer">{timeString}</div>
          </div>
        </div>
      </div>
      <BackTop duration={100} style={style} />
    </>
  );
};
