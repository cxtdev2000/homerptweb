
import React, { useEffect } from 'react';
import {
    removeAuthenFromStorage, removeDataEncryptFromStorage, removeLanguageFromStorage,
    removeUserFromStorage
} from "../redux/actions/sharedActions";

function Logout() {
    
    useEffect(()=>{
        removeAuthenFromStorage();
        removeLanguageFromStorage();
        removeUserFromStorage();
        removeDataEncryptFromStorage();
    },[]);

    return (
        <div>
            <span style={{color:'red'}}>Logout</span>            
        </div>
    );
}

export default Logout;