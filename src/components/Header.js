// Author : Can Xuan Tung – Navisoft.
import { NavLink, useLocation } from "react-router-dom";
import { useHeaderMenuList, useHeaderAccountMenuList } from "./HeaderMenuList";
import { useState, useEffect } from "react";
import { t } from "../translations/T";

function Header(props) {
  const location = useLocation();
  const HeaderMenuList = useHeaderMenuList(t);
  const HeaderAccountMenuList = useHeaderAccountMenuList(t);

  // lưu menu và menu con
  const [clickedMenu, setClickedMenu] = useState({});
  const [menu, setMenu] = useState({});
  const [subMenu, setSubMenu] = useState([]);
  const [subMenuChoosen, setSubMenuChoosen] = useState({});
  const [subMenulv2, setSubMenulv2] = useState([]);
  const [subMenulv2Choosen, setSubMenulv2Choosen] = useState({});

  const [prevOrder, setPrevOrder] = useState(0);
  //const [menuDraw, setMenuDraw] = useState([]);
  // end

  useEffect(() => {
    let headerMenu = [];
    function draw(level, obj, parent = null) {
      headerMenu.push({ level: level, parent: parent, ...obj });

      if (obj.children != undefined) {
        obj.children.forEach((s) => draw(level + 1, s, obj.uri)); //invoke draw in a loop
        //draw(obj.children[0]); //use
      }
    }
    HeaderMenuList.forEach((element) => {
      draw(1, element);
    });

    let currentMenu = headerMenu.find((e) => e.uri === location.pathname);

    if (currentMenu?.level == 3) {
      let currentMenuLv2 = headerMenu.find(
        (e) => e.level === 2 && e.uri === currentMenu.parent,
      );
      let currentMenuLv1 = headerMenu.find(
        (e) => e.level === 1 && e.uri === currentMenuLv2.parent,
      );

      setMenu(currentMenuLv1);
      setSubMenu(currentMenuLv1.children);

      setSubMenuChoosen(currentMenuLv2);
      setSubMenulv2(currentMenuLv2.children);

      setSubMenulv2Choosen(currentMenu);
    }
    if (currentMenu?.level == 2) {
      let currentMenuLv1 = headerMenu.find(
        (e) => e.level === 1 && e.uri === currentMenu.parent,
      );

      setMenu(currentMenuLv1);
      setSubMenu(currentMenuLv1.children);

      setSubMenuChoosen(currentMenu);
      setSubMenulv2(currentMenu.children);
    }

    return () => {};
  }, []);
  return (
    <>
      <div className="header">
        <div className="">
          <div className="flex-header">
            {/* HTML head left */}
            <div className="head-left">
              <div className="logo">
                <a className="logo__link">
                  <img src="/image/logo-light.svg" alt="" />
                </a>
              </div>
              {/* HTML navigation begin */}
              <div className="navigation">
                <ul className="menu-parent">
                  {/* Menu cấp 1 */}
                  {HeaderMenuList.map((e) => (
                    <li
                      key={JSON.stringify(e)}
                      className={menu.uri === e.uri ? "active" : ""}
                    >
                      <NavLink
                        exact
                        to={e.children?.length > 0 ? e.children[0]?.uri : e.uri}
                        onClick={() => {
                          setClickedMenu(e);
                          setMenu(e);
                          setSubMenu(e.children);
                          setSubMenuChoosen({});
                          setSubMenulv2Choosen({});
                        }}
                      >
                        <img className="icon-nav" src={e.icon} alt="img" />
                        <img className="icon-nav-ac" src={e.iconFill} alt="" />
                        <span>{e.text}</span>
                      </NavLink>
                    </li>
                  ))}
                </ul>
              </div>
              {/* End HTML navigation */}
            </div>
            {/* End */}
            {/* HTML head right */}
            <div className="head-right">
              {/* HTML head search */}
              <div className="head-right-search">
                <input type="text" placeholder="Mã giao dịch" />
              </div>
              {/* End */}
              {/* HTML bell */}
              <div className="bell">
                <a href="">
                  <img src="/image/icon/ic-bell.svg" alt="" />
                </a>
              </div>
              {/* End */}
              {/* HTML account users */}
              <div className="user">
                <div className="avt">
                  <img src="/image/user.png" alt="img" />
                </div>
                <div className="block-utility">
                  <div className="box-utility">
                    {HeaderAccountMenuList.map((e) => (
                      <a key={e.uri} href={e.uri}>
                        <span>{e.text}</span>
                      </a>
                    ))}
                  </div>
                </div>
              </div>
              {/* End account HTML*/}
            </div>
            {/* End */}
          </div>
        </div>
        {/* HTML navigation sub */}
        <div
          className="sub-head"
          // onMouseLeave={() => setSubMenu([])}
        >
          <div className="">
            <div className="navigation sub">
              <ul className="menu-parent">
                {/* Menu cấp 2  */}
                {subMenu.length > 0 &&
                  subMenu?.map((e, i) => (
                    <li
                      key={new Date().getTime() + JSON.stringify(e)}
                      className={`has-dropdown ${
                        (subMenuChoosen.uri === e.uri ||
                          location.pathname === e.uri) &&
                        "active"
                      }`}
                    >
                      <NavLink
                        exact
                        state={{ order: i, prevOrder: prevOrder }}
                        to={e.uri}
                        onClick={() => {
                          setSubMenuChoosen(e);
                          setSubMenulv2(e.children);
                          setPrevOrder(i);
                        }}
                      >
                        <span>{e.text}</span>
                        {/* Có menu con cấp 3 -> hiện mũi tên  */}
                        {e.children.length > 0 && (
                          <img
                            className="icon-agldown"
                            src="/image/icon/ic-arr-down.svg"
                            alt="img"
                          />
                        )}
                      </NavLink>
                      {/* Có menu con cấp 3 -> hiện dropdown menu con  */}
                      {e.children.length > 0 && (
                        <div className="block-dropdown">
                          <ul className="menuchild">
                            {/* Menu cấp 3  */}
                            {e.children?.map((i) => (
                              <li
                                key={new Date().getTime() + JSON.stringify(i)}
                              >
                                <NavLink exact to={i.uri}>
                                  <span></span>
                                  {i.text}
                                </NavLink>
                              </li>
                            ))}
                          </ul>
                        </div>
                      )}
                    </li>
                  ))}
              </ul>
            </div>
          </div>
        </div>
        {/* End */}
      </div>
    </>
  );
}

export default Header;
