import { useTranslation } from "react-i18next";
import Header from "./Header";
import { Footer } from "./Footer";

// Layout chưa có tài khoản
export const Layout = ({children}) => {
  const { t } = useTranslation();

  return (
    <div className="wrapper">
      <Header />
      <Footer />
      {children}
    </div>
  );
};
