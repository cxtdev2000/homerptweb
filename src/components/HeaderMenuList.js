// mảng json chứa header menu, hiển thị ngay đầu web nhé
// role: mảng chứa các id role đc phép truy cập (có * là tất cả)
// permission: id per đc phép truy cập (có * là all)

export const useHeaderMenuList = (t) => {
  return [
    // quản lý người dùng
    {
      text: t("Giám sát chung"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/dashboard",
      route: "",
      role: [],
      permission: [],
      children: []
    },
    {
      text: t("Q.Lý người dùng hệ thống"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/dashboard/staff",
      route: "",
      role: [],
      permission: [],
      children: [
        {
          text: t("qLyNguoiDung"),
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/user",
          route: "",
          role: ["super-admin", "admin"],
          permission: [],
          children: [],
        },
        {
          text: t("Quản lý nhóm người sử dụng"),
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/group",
          route: "",
          role: ["super-admin", "admin"],
          permission: [],
          children: [],
        },
        {
          text: t("Quản lý chức quyền"),
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/role",
          route: "",
          role: ["super-admin"],
          permission: [],
          children: [],
        },
      ],
      component: null,
    },
    // qly tham số hệ thống
    {
      text: t("qLyThamSoHeThong"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/dashboard/systemparams",
      route: "",
      role: [],
      permission: [],
      children: [
        {
          text: "Q.lý vùng miền",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/zone",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
        {
          text: "Q.lý tỉnh/thành phố",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/province",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
        {
          text: "Q.lý quận/huyện",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/district",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
        {
          text: "Q.lý xã/phường",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/ward",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
      ],
    },
    // giám sát
    {
      text: t("Giám sát và quản lý hệ thống thiết bị"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/dashboard/controling",
      route: "",
      role: [],
      permission: [],
      children: [
        {
          text: "Loại thiết bị",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/devicetype",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
        {
          text: "Cấu hình",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/config",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
        {
          text: "Thiết bị",
          icon: "/image/icon/ic-nav.svg",
          iconFill: "/image/icon/ic-nav-fill.svg",
          uri: "/dashboard/device",
          route: "",
          role: [],
          permission: [],
          children: [],
        },
      ],
    },
  ];
};
export const useHeaderAccountMenuList = (t) => {
  return [
    // đổi mật khẩu
    {
      text: t("doiMatKhau"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/changepassword",
      route: "",
      role: [],
      permission: [],
      children: [],
    },
    // hướng dẫn
    {
      text: t("huongDan"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/tutorial",
      route: "",
      role: [],
      permission: [],
      children: [],
    },
    // đăng xuất
    {
      text: t("dangXuat"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/logout",
      route: "",
      role: [],
      permission: [],
      children: [],
    },
    // thoát khỏi CT
    {
      text: t("thoatKhoiCT"),
      icon: "/image/icon/ic-nav.svg",
      iconFill: "/image/icon/ic-nav-fill.svg",
      uri: "/exit",
      route: "",
      role: [],
      permission: [],
      children: [],
    },
  ];
};
