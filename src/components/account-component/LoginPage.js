// Author : Can Xuan Tung – Navisoft.
import { useTranslation } from "react-i18next";
import {
  Route,
  Navigate,
  Outlet,
  NavLink,
  useNavigate,
  useSearchParams,
} from "react-router-dom";
import { useState, createContext, useContext } from "react";
import { useSelector } from "react-redux";
import { login } from "../../services/oauth";
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
} from "antd";

export const LoginPage = (props) => {
  const { t } = useTranslation();
  let navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();

  const [isLoading, setIsLoading] = useState(false);
  const user = useSelector((state) => state.auth.user);
  const [Username, setUsername] = useState("");
  const [Password, setPassword] = useState("");

  const handleSubmitLogin = () => {
    setIsLoading(true);
    login({ email: Username, password: Password })
      .then((token) => {
        if (token) {
          notification.success({
            message: `Đăng nhập thành công`,
            description: `Xin chào ${Username}`,
          });
          return navigate(searchParams.get("redirect") ?? "/dashboard/user");
        } else {
          notification.error({
            message: `Thất bại`,
            description: `Đăng nhập thất bại, tài khoản hoặc mật khẩu không đúng`,
          });
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log(error);
        notification.error({
          message: `Thất bại`,
          description: `Lỗi kết nối hệ thống`,
        });
        setIsLoading(false);
      });
  };
  return user ? (
    <Navigate to={searchParams.get("redirect") ?? "/dashboard/user"} />
  ) : (
    <div className="wrap__login">
      <div className="container">
        <div className="flex-login">
          <div className="flex-head">
            <a href="#">
              <img src="/image/icon/ic-logo-login.svg" alt="page-logo" />
            </a>
            <span>Công ty Cổ phần Giải pháp và Dịch vụ phần mềm Nam Việt</span>
          </div>
          <div className="flex-body">
            <Spin spinning={isLoading} tip={`Đang đăng nhập...`}>
              <div className="box-login">
                <h2>Hệ thống quản lý Home Repeater</h2>
                <h3>Đăng Nhập</h3>
                <div className="block-login">
                  <form action="javascript:;">
                    <div className="field-login">
                      <input
                        className="w-100"
                        type="text"
                        placeholder="Tài khoản"
                        required
                        autoComplete="off"
                        onChange={(e) => {
                          setUsername(e.target.value);
                        }}
                      />
                      <img src="/image/account.svg" alt="" />
                    </div>
                    <div className="field-login">
                      <input
                        className="w-100"
                        type="password"
                        placeholder="Mật khẩu"
                        required
                        autoComplete="off"
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                      />
                      <img src="/image/password.svg" alt="" />
                    </div>
                    <div className="save-password">
                      <input type="checkbox" id="checkbox-save" />
                      <label htmlFor="checkbox-save">Ghi nhớ mật khẩu</label>
                    </div>
                    <button
                      className="btn-login"
                      onClick={() => {
                        handleSubmitLogin();
                      }}
                    >
                      Đăng nhập
                    </button>
                  </form>
                </div>
              </div>
            </Spin>
          </div>
          <div className="flex-footer">
            <div className="copyright">
              <p>
                Copy right 2022. Bản quyền thuộc về Công ty Cổ phần Giải pháp và
                Dịch vụ phần mềm Nam Việt
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
