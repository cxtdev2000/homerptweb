// Author : Can Xuan Tung – Navisoft.
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  DatePicker,
  InputNumber,
  Divider,
} from "antd";
import { useGlobalConst } from "../../../../helpers/constData";
import { t } from "i18next";
import { useSelector } from "react-redux";
import * as apiHelper from "../../../../helpers/apiHelper";
import { useEffect, useState } from "react";

const { Option } = Select;
const InputItems = ({
  formValues,
  allcode,
  formItemLayout,
  inquiry,
  isEditing = false,
}) => {
  const globalConst = useGlobalConst(t);
  const [provinces, setProvinces] = useState([]);
  const [districts, setDistricts] = useState([]);
  const [wards, setWards] = useState([]);


  useEffect(() => {
    apiHelper
      .getProvinces()
      .then((res) => {
        if (res.status === 200) {
          setProvinces(res.data);
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });

    apiHelper
      .getDistricts()
      .then((res) => {
        if (res.status === 200) {
          setDistricts(res.data);
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
    apiHelper
      .getWards()
      .then((res) => {
        if (res.status === 200) {
          setWards(res.data);
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
  }, []);

  return (
    <>
      <Form.Item hidden name={"AUTOID"}>
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Mã thiết bị`}
        name={"code"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Tên thiết bị`}
        name={"name"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Toạ độ`}
        name={"name"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Nhóm thiết bị`}
        name={"group"}
      >
        <Select
          className="cs-ant-select"
          showSearch
          mode="single"
          placeholder={`${t("luaChon")}`}
          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        >
          <Option value={"server"}>Máy chủ</Option>
          <Option value={"repeater"}>Repeater</Option>
          <Option value={"tempsensor"}>Cảm biến nhiệt</Option>
          <Option value={"drysensor"}>Cảm biến độ ẩm</Option>
        </Select>
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Nhân viên quản lý`}
        name={"group"}
      >
        <Select
          className="cs-ant-select"
          showSearch
          mode="single"
          placeholder={`${t("luaChon")}`}
          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        >
          <Option value={"admin"}>admin</Option>
        </Select>
      </Form.Item>

      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Tỉnh/thành phố`}
        name={"provinceCode"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
        ]}
      >
        <Select
          className="cs-ant-select"
          showSearch
          mode="single"
          placeholder={`${t("luaChon")}`}
          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        >
          {provinces?.map((e) => (
            <Option key={"provinceCode" + e.id} value={e?.id}>
              {e?.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Quận/huyện`}
        name={"districtCode"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
        ]}
      >
        <Select
          className="cs-ant-select"
          showSearch
          mode="single"
          placeholder={`${t("luaChon")}`}
          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        >
          {districts?.map((e) => (
            <Option key={"provinceCode" + e.id} value={e?.id}>
              {e?.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Phường xã`}
        name={"wardCode"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
        ]}
      >
        <Select
          className="cs-ant-select"
          showSearch
          mode="single"
          placeholder={`${t("luaChon")}`}
          filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
        >
          {wards?.map((e) => (
            <Option key={"provinceCode" + e.id} value={e?.id}>
              {e?.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
    </>
  );
};
export default InputItems;
