// Author : Can Xuan Tung – Navisoft.
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  DatePicker,
  InputNumber,
  Divider,
} from "antd";
import { useGlobalConst } from "../../../../helpers/constData";
import { t } from "i18next";
import { useSelector } from "react-redux";
import * as apiHelper from "../../../../helpers/apiHelper";
import { useEffect, useState } from "react";

const { Option } = Select;
const InputItems2 = ({
  formValues,
  allcode,
  formItemLayout,
  inquiry,
  isEditing = false,
}) => {
  const globalConst = useGlobalConst(t);

  return (
    <>
      <Form.Item hidden name={"AUTOID"}>
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`TX Input limit power`}
        name={"code0"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`TX output over power`}
        name={"name1"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`TX output under power`}
        name={"name3"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`RX output over power`}
        name={"name4"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`TX/RX amp on/off`}
        name={"name5"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Over temp limit`}
        name={"name6"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Inside temp*`}
        name={"name7"}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
    </>
  );
};
export default InputItems2;
