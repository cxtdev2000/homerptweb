// Author : Can Xuan Tung – Navisoft.
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  DatePicker,
  InputNumber,
  Divider,
} from "antd";
import { useGlobalConst } from "../../../../helpers/constData";
import { t } from "i18next";
import { useSelector } from "react-redux";
import * as apiHelper from "../../../../helpers/apiHelper";
import { useEffect, useState } from "react";

const { Option } = Select;
const InputItems = ({
  formValues,
  allcode,
  formItemLayout,
  isEditing = false,
}) => {
  const globalConst = useGlobalConst(t);
  const lang = useSelector((state) => state.language.language);
  const [groups, setGroups] = useState([]);

  useEffect(() => {
    apiHelper
      .getGroups()
      .then((res) => {
        if (res.status === 200) {
          setGroups(res.data);
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
    return () => {};
  }, []);

  return (
    <div className="container">
      <div className="head-add pt-0">{`${t("thongTinNguoiDung")}`}</div>
      <div className="row justify-content-between">
        <div className="col-md-8">
          <div className="box-add">
            <div className="group-add">
              {/* Mã  */}
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("maNguoiDung")}`}
                name={"code"}
                rules={[
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                  globalConst.ANT.FORM.RULES.yeuCauNhap,
                  globalConst.ANT.FORM.RULES.gioiHan20KiTu,
                ]}
              >
                <Input disabled={isEditing} className="cs-ant-input" />
              </Form.Item>
              {/* <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("trangThai")}`}
                name={"active"}
                rules={[globalConst.ANT.FORM.RULES.khongKhoangTrang]}
              >
                <Select
                  className="cs-ant-select"
                  mode="single"
                  placeholder={`${t("luaChon")}`}
                >
                  <Option value={true}>Kích hoạt</Option>
                  <Option value={false}>Không kích hoạt</Option>
                </Select>
              </Form.Item> */}
            </div>
            <div className="group-add">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search mw-100"
                label={`${t("tenNguoiDung")}`}
                name={"fullName"}
                rules={[
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                  globalConst.ANT.FORM.RULES.yeuCauNhap,
                  globalConst.ANT.FORM.RULES.gioiHan200KiTu,
                ]}
              >
                <Input className="cs-ant-input mw-100" />
              </Form.Item>
            </div>
            <div className="group-add">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("soDienThoai")}`}
                name={"phoneNumber"}
                rules={[
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                  globalConst.ANT.FORM.RULES.chiDuoc10KiTu,
                  globalConst.ANT.FORM.RULES.chiNhapSo,
                ]}
              >
                <Input className="cs-ant-input" />
              </Form.Item>
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`Ngày sinh`}
                name={"birthdate"}
                rules={[globalConst.ANT.FORM.RULES.khongKhoangTrang]}
                {...globalConst.ANT.FORM.ITEM.PARSER.DATE_MOMENT}
              >
                <DatePicker
                  className="cs-ant-picker"
                  format={globalConst.ANT.LOCALE.dateFormat}
                  placeholder={""}
                />
              </Form.Item>
            </div>
            <div className="group-add">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("email")}`}
                name={"email"}
                rules={[
                  globalConst.ANT.FORM.RULES.email,
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                ]}
              >
                <Input className="cs-ant-input" />
              </Form.Item>
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("Chức quyền")}`}
                name={"role"}
                rules={[globalConst.ANT.FORM.RULES.khongKhoangTrang]}
              >
                <Select
                  className="cs-ant-select"
                  showSearch
                  mode="single"
                  placeholder={`${t("luaChon")}`}
                  filterOption={(input, option) =>
                    option.children.toLowerCase().includes(input.toLowerCase())
                  }
                >
                  <Option value="super-admin">Super Admin</Option>
                  <Option value="admin">Admin</Option>
                  <Option value="nhan-vien">Nhân viên</Option>
                  <Option value="giam-sat">Giám sát</Option>
                </Select>
              </Form.Item>
            </div>
          </div>
        </div>
        {/* <div className="col-md-1" /> */}
        <div className="col-md-4" style={{ paddingLeft: "80px" }}>
          <div className="box-add">
            <div className="group-add">
              <div className="item-search"></div>
            </div>
            <div className="group-add justify-content-end">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("taiKhoan")}`}
                name={"userName"}
                rules={[
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                  globalConst.ANT.FORM.RULES.yeuCauNhap,
                  globalConst.ANT.FORM.RULES.gioiHan20KiTu,
                ]}
              >
                <Input disabled={isEditing} className="cs-ant-input" />
              </Form.Item>
            </div>
            <div className="group-add justify-content-end">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`${t("matKhau")}`}
                name={"PasswordHash"}
                rules={[
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                  globalConst.ANT.FORM.RULES.yeuCauNhap,
                  globalConst.ANT.FORM.RULES.gioiHan20KiTu,
                ]}
              >
                <Input className="cs-ant-input" />
              </Form.Item>
            </div>
            <div className="group-add justify-content-end">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search"
                label={`Nhóm người sử dụng`}
                name={"UserGroups"}
                rules={[globalConst.ANT.FORM.RULES.khongKhoangTrang]}
                normalize={(value) => value?.map((e) => JSON.parse(e))}
                getValueProps={(value) => value?.map((e) => JSON.stringify(e))}
              >
                <Select
                  className="cs-ant-select"
                  mode="multiple"
                  placeholder={`${t("luaChon")}`}
                >
                  {groups?.map((e) => (
                    <Option value={JSON.stringify(e)}>{e?.name}</Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>
        </div>
        <div className="col-md-12">
          <div className="box-add mr-3">
            <div className="group-add">
              <Form.Item
                {...formItemLayout}
                className="cs-ant-formitem item-search mw-100"
                label={`${t("ghiChu")}`}
                name={"description"}
                rules={[
                  globalConst.ANT.FORM.RULES.khongKhoangTrang,
                  globalConst.ANT.FORM.RULES.gioiHan200KiTu,
                ]}
              >
                <Input className="cs-ant-input mw-100" />
              </Form.Item>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default InputItems;
