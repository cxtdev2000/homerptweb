// Author : Can Xuan Tung – Navisoft.
import { Badge, Descriptions, Select, Space } from "antd";
import Moment from "react-moment";
import "../style.css";

const { Option } = Select;
const DetailsView = ({ record, inquiry }) => {
  return (
    <Space direction="vertical" size="middle" style={{ display: "flex" }}>
      {/* Thông tin chung  */}
      <Descriptions title="Thông tin chung" bordered size={"small"}>
        <Descriptions.Item label={`Mã ngân hàng`} span={3}>
          {record?.BANKCODE}
        </Descriptions.Item>
        <Descriptions.Item label={`Tên ngân hàng`} span={2}>
          {record?.BANKNAME}
        </Descriptions.Item>
        <Descriptions.Item label={`Tên viết tắt`} span={1}>
          {record?.SHORTNAME}
        </Descriptions.Item>
        <Descriptions.Item label={`Mô tả`} span={3}>
          {record?.DESCRIPTION}
        </Descriptions.Item>
        <Descriptions.Item label={`Trạng thái" span={3}>
          <Badge
            status={
              ConstSA?.STATUS_SET?.find((e) => e.code == record?.["STATUS"])
                ?.badge
            }
            text={
              inquiry?.filterList
                ?.find((e) => e.key === "STATUS")
                ?.options?.find((e) => e.cdVal == record?.["STATUS"])?.content
            }
          />
        </Descriptions.Item>
        {inquiry?.filterList
          ?.find((e) => e.key === "STATUS")
          ?.options?.find((e) => e.cdVal == record?.["STATUS"])?.cdVal ==
          STATUS_REFUSE && (
          <Descriptions.Item label={`Lý do từ chối span={3}>
            <i>{record?.REJECTDES}</i>
          </Descriptions.Item>
        )}
      </Descriptions>

      {/* Liên hệ  */}
      <Descriptions title="Liên hệ" bordered size={"small"}>
        <Descriptions.Item label={`Địa chỉ`} span={3}>
          {record?.ADDRESS}
        </Descriptions.Item>
        <Descriptions.Item label={`Số điện thoại" span={1}>
          {record?.PHONE}
        </Descriptions.Item>
        <Descriptions.Item label={`Fax`} span={2}>
          {record?.FAX}
        </Descriptions.Item>
        <Descriptions.Item label={`Website`} span={3}>
          {record?.WEBSITE}
        </Descriptions.Item>
      </Descriptions>

      {/* TIMELINE trạng thái */}
      <Descriptions title="Lịch sử trạng thái" bordered size={"small"}>
        <Descriptions.Item label={`Ngày tạo" span={1}>
          <Moment format="DD/MM/YYYY HH:MM:SS">{record?.CREATEDATE}</Moment>
        </Descriptions.Item>
        <Descriptions.Item label={`Người tạo" span={2}>
          {record?.CREATEBY}
        </Descriptions.Item>
      </Descriptions>
    </Space>
  );
};
export default DetailsView;
