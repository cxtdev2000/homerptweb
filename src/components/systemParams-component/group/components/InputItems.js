// Author : Can Xuan Tung – Navisoft.
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  DatePicker,
  InputNumber,
  Divider,
} from "antd";
import { useGlobalConst } from "../../../../helpers/constData";
import { t } from "i18next";
import { useSelector } from "react-redux";
import * as apiHelper from "../../../../helpers/apiHelper";
import { useEffect, useState } from "react";

const { Option } = Select;
const InputItems = ({
  formValues,
  allcode,
  formItemLayout,
  inquiry,
  isEditing = false,
}) => {
  const globalConst = useGlobalConst(t);
  const [wards, setDistricts] = useState([]);

  useEffect(() => {
    apiHelper
      .getDistricts()
      .then((res) => {
        if (res.status === 200) {
          setDistricts(res.data);
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
    return () => {};
  }, []);

  return (
    <>
      <Form.Item hidden name={"AUTOID"}>
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Mã nhóm người sử dụng`}
        name={"id"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
          globalConst.ANT.FORM.RULES.gioiHan200KiTu,
        ]}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Tên nhóm người sử dụng`}
        name={"name"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
          globalConst.ANT.FORM.RULES.gioiHan200KiTu,
        ]}
      >
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`${t("ghiChu")}`}
        name={"description"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.gioiHan200KiTu,
        ]}
      >
        <Input className="cs-ant-input mw-100" />
      </Form.Item>
    </>
  );
};
export default InputItems;
