import { Badge, Dropdown, Menu, Space, Table, Tooltip, Switch } from "antd";
import { useSelector, useDispatch } from "react-redux";
import Moment from "react-moment";
import { useGlobalConst } from "../../helpers/constData";

//render giá trị

const DropDownRender = ({ children, tooltip }) => {
  return (
    <Tooltip placement="topLeft" title={tooltip}>
      <div>{children}</div>
    </Tooltip>
  );
};
const Cell = ({ children }) => {
  return <div className="cell-content">{children}</div>;
};
export const SaStatusTextRender = ({ statusText, status, rejectDes }) => {
  const globalConst = useGlobalConst();
  let colorClass = "";

  if (status == globalConst.APP.CODE.STATUS.APPROVED) colorClass = "clr-ac";
  if ([globalConst.APP.CODE.STATUS.PENDING].includes(status)) {
    colorClass = "clr-yl";
  }

  // nếu từ chối hiển thị tooltip lý do từ chối
  if (status == globalConst.APP.CODE.STATUS.REJECTED) {
    colorClass = "clr-red";
    return (
      <DropDownRender tooltip={rejectDes}>
        <p className={colorClass}>{statusText}</p>
      </DropDownRender>
    );
  }
  // return
  return <p className={colorClass}>{statusText}</p>;
};
export const IshoRender = ({ text }) => {
  if (text == "Y") text = "Hội Sở";
  if (text == "N") text = "Chi nhánh";

  return <p>{text}</p>;
};
export const MapColumns = (columnList, handleActive) => {
  return columnList.map((e) => {
    let render = (text) => <Cell>{text}</Cell>;

    //Lọc theo trường dữ liệu
    if (e.key === "WEBSITE")
      render = (text) => (
        <>
          <Cell>
            <a href={text} target="_blank">
              {text}
            </a>
          </Cell>
        </>
      );
    if (e.key === "ISHO")
      render = (text) => (
        <>
          <Cell>
            <IshoRender text={text} />
          </Cell>
        </>
      );
    if (e.key === "STATUSTEXT")
      render = (text, record) => (
        <Cell>
          <SaStatusTextRender
            statusText={text}
            status={record.STATUS}
            rejectDes={record.REJECTDES}
          />
        </Cell>
      );
    if (e.key === "ADDRESS")
      render = (text, record) => (
        <Cell>
          <Tooltip placement="topLeft" title={record.ADDRESS}>
            {text}
          </Tooltip>
        </Cell>
      );
    if (e.key === "DESCRIPTION")
      render = (text, record) => (
        <Cell>
          <Tooltip placement="topLeft" title={record.DESCRIPTION}>
            {text}
          </Tooltip>
        </Cell>
      );
    if (e.key === "REJECTDES")
      render = (text, record) => (
        <Cell>
          <Tooltip placement="topLeft" title={record.REJECTDES}>
            {text}
          </Tooltip>
        </Cell>
      );
    if (e.key === "administrativeRegion")
      render = (text, record) => <Cell>{text?.name}</Cell>;
    if (e.key === "provinceCodeNavigation")
      render = (text, record) => <Cell>{text?.fullName}</Cell>;
    if (e.key === "districtCodeNavigation")
      render = (text, record) => (
        <Cell>
          {text?.provinceCodeNavigation?.fullName} - {text?.fullName}
        </Cell>
      );
    // Lọc theo kiểu dữ liệu
    if (e.dataType === "DAT" || e.dataType === "DAT10") {
      render = (text) => (
        <>
          <Moment format="DD/MM/YYYY">{text}</Moment>
        </>
      );
    }
    if (e.dataType === "DAT16") {
      render = (text) => (
        <>
          <Moment format="DD/MM/YYYY HH:mm">{text}</Moment>
        </>
      );
    }
    if (e.dataType === "DAT19") {
      render = (text) => (
        <>
          <Moment format="DD/MM/YYYY HH:mm:ss">{text}</Moment>
        </>
      );
    }
    if (e.dataType === "SWITCH") {
      render = (text, record) => (
        <>
          <Switch
            // checkedChildren="Hoạt động"
            // unCheckedChildren="Nghỉ"
            defaultChecked={text}
            onChange={(checked, e) => handleActive(checked, record.code)}
          />
        </>
      );
    }

    return {
      ...e,
      dataIndex: e.key,
      ellipsis: {
        showTitle: false,
      },
      render: render,
    };
  });
};
