// Author : Can Xuan Tung – Navisoft.
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  DatePicker,
  InputNumber,
  Divider,
} from "antd";
import { useGlobalConst } from "../../../../helpers/constData";
import { t } from "i18next";
import { useSelector } from "react-redux";
import * as apiHelper from "../../../../helpers/apiHelper";
import { useEffect, useState } from "react";

const { Option } = Select;
const InputItems = ({
  formValues,
  allcode,
  formItemLayout,
  inquiry,
  isEditing = false,
}) => {
  const globalConst = useGlobalConst(t);
  const [regions, setRegions] = useState([]);

  useEffect(() => {
    apiHelper
      .getRegions()
      .then((res) => {
        if (res.status === 200) {
          setRegions(res.data);
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
    return () => {};
  }, []);

  return (
    <>
      <Form.Item hidden name={"AUTOID"}>
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Mã tỉnh`}
        name={"code"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
          globalConst.ANT.FORM.RULES.gioiHan200KiTu,
        ]}
      >
        <Input disabled={isEditing} className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Tên tỉnh/tp`}
        name={"fullName"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
          globalConst.ANT.FORM.RULES.gioiHan200KiTu,
        ]}
      >
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Vùng/miền`}
        name={"administrativeRegionId"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
        ]}
      >
        <Select
          className="cs-ant-select"
          mode="single"
          placeholder={`${t("luaChon")}`}
        >
          {regions?.map((e) => (
            <Option key={"administrativeRegionId" + e.id} value={e?.id}>
              {e?.name}
            </Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`Zip code`}
        name={"zipCode"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.yeuCauNhap,
          globalConst.ANT.FORM.RULES.gioiHan20KiTu,
        ]}
      >
        <Input className="cs-ant-input" />
      </Form.Item>
      <Form.Item
        {...formItemLayout}
        className="cs-ant-formitem"
        label={`${t("moTa")}`}
        name={"description"}
        rules={[
          globalConst.ANT.FORM.RULES.khongKhoangTrang,
          globalConst.ANT.FORM.RULES.gioiHan500KiTu,
        ]}
      >
        <Input.TextArea className="cs-ant-textarea" autoSize />
      </Form.Item>
    </>
  );
};
export default InputItems;
