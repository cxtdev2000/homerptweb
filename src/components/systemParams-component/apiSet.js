import { useAxios } from "../../helpers/apiHelper";
import { t } from "i18next";

export const useHttpRequest = (controller) => {
  const httpRequest = useAxios();

  return {
    inquiry: (inquiry) => {
      return httpRequest.post(`api/${controller}`, inquiry, {
        responseType: inquiry.IsExport ? "arraybuffer" : "",
      });
    },
    // getInquiry: (sCode) => {
    //   return httpRequest.get(`api/Search/getbycode?code=${sCode}`);
    // },
    getDetailsByAutoId: (id) => {
      return httpRequest.get(`/api/${controller}/getbyautoid?autoId=${id}`);
    },
    addOne: (jsonData) => {
      return httpRequest.post(`/api/${controller}/addone`, jsonData);
    },
    updateOne: (jsonData) => {
      return httpRequest.post(`/api/${controller}/updateone`, jsonData);
    },
    deleteOne: (jsonData) => {
      return httpRequest.post(`/api/${controller}/deleteone`, jsonData);
    },
    activeOne: (jsonData) => {
      return httpRequest.post(`/api/${controller}/activeone`, jsonData);
    },
  };
};
