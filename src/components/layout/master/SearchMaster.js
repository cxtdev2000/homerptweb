import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  Dropdown,
  Menu,
  Skeleton,
} from "antd";
import ReactDragListView from "react-drag-listview";
import { useEffect, useState, useMemo } from "react";
import { useTranslation } from "react-i18next";
import { NavLink } from "react-router-dom";
import InquiryForm from "../../layout/table/Inquiry";
import Pagination from "../../layout/table/Pagination";
import { MapColumns } from "../../systemParams-component/dataSetCollection";
import { useGlobalConst } from "../../../helpers/constData";
import { useHttpRequest } from "../../systemParams-component/apiSet";
import { useSelector } from "react-redux";
import Joyride, { CallBackProps, STATUS, Step } from "react-joyride";
import ReactPlayer from "react-player/lazy";
import FormMaster from "./FormMaster";
import moment from "moment";
import { t } from "i18next";

export const SearchMaster = ({ config }) => {
  const { t } = useTranslation();
  const globalConst = useGlobalConst(t);

  const CONTROLLER = useMemo(
    () => globalConst.MODULE[config.module][config.constName].CONTROLLER,
    [globalConst],
  );
  const inquiry = useMemo(
    () => globalConst.MODULE[config.module][config.constName].SEARCH.INQUIRY,
    [globalConst],
  );

  const lang = useSelector((state) => state.language.language);
  const Axios = useHttpRequest(CONTROLLER);
  //#region Joyride
  const JDLocale = {
    skip: (
      <strong className="text-white" aria-label="skip">
        Bỏ qua
      </strong>
    ),
    next: (
      <strong className="text-white" aria-label="skip">
        Tiếp theo
      </strong>
    ),
    last: (
      <strong className="text-white" aria-label="skip">
        Đã rõ!
      </strong>
    ),
    back: (
      <strong className="text-white" aria-label="skip">
        Quay lại
      </strong>
    ),
  };
  const [jDStep, setJDStep] = useState({
    run: false,
    steps: [
      {
        content: (
          <h3>{`Hướng dẫn sử dụng chức năng tra cứu ${
            globalConst.MODULE[config.module][config.constName].CATEGORY_VN
          }`}</h3>
        ),
        locale: JDLocale,
        placement: "center",
        target: "body",
      },
      // Hướng dẫn chức năng thêm mới sửa xoá...
      {
        content: (
          <>
            <h3>Các chức năng tương tác nghiệp vụ</h3>
            <p>Khu vực bao gồm các nút tương tác nghiệp vụ với dữ liệu</p>
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".records-actions",
      },
      // Hướng dẫn chức năng filter
      {
        content: (
          <>
            <h3>Khu vực điều kiện truy vấn</h3>
            <p>
              Nơi nhập dữ liệu tìm kiếm, gồm trường tìm kiếm, độ khớp, giá trị
              tìm kiếm, bên phải là danh sách truy vấn đã chọn
            </p>
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".search__wrap",
      },
      // Hướng dẫn nhập dữ liệu filter
      {
        content: (
          <>
            <h3>Bộ dữ liệu truy vấn</h3>
            <p>
              Bộ dữ liệu nhập gồm trường tìm kiếm, độ khớp LIKE, =, giá trị tìm
              kiếm
            </p>
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".search__option",
      },
      // Danh sách điều kiện tìm kiếm
      {
        content: (
          <>
            <h3>Danh sách điều kiện tìm kiếm</h3>
            <p>
              Danh sách điều kiện tìm kiếm được tích sẵn sau khi bấm nút thêm
              điều kiện
            </p>
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".search__list",
      },
      // Khu vực kết quả truy vấn
      {
        content: (
          <>
            <h3>Khu vực kết quả truy vấn</h3>
            <p>Nơi hiển thị kết quả truy vấn sau khi bấm nút tìm kiếm</p>
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".cs-ant-table",
      },
      // Sắp xếp cột
      {
        content: (
          <>
            <h3>Sắp xếp một cột</h3>
            <p>
              Sắp xếp một cột bằng cách bấm vào cột cần sắp xếp, quy trình sắp
              xếp sẽ theo thứ tự: Xuôi, ngược, mặc định
            </p>
            <ReactPlayer
              url="./media/sort.mov"
              playing
              loop
              playbackRate={1.5}
              width="100%"
              height="100%"
            />
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".cs-ant-table",
      },
      // Kéo thả vị trí cột
      {
        content: (
          <>
            <h3>Kéo thả vị trí các cột</h3>
            <p>
              Kéo thả vị trí các cột bằng cách kéo một cột rồi thả vào vị trí
              mong muốn, thứ tự sắp xếp sẽ không mất khi tải lại trang hoặc đăng
              nhập lại
            </p>
            <ReactPlayer
              url="./media/dragcolumn.mov"
              playing
              loop
              playbackRate={1}
              width="100%"
              height="100%"
            />
          </>
        ),
        locale: JDLocale,
        floaterProps: {
          disableAnimation: true,
        },
        spotlightPadding: 20,
        target: ".cs-ant-table",
      },
    ],
  });
  const handleJoyrideCallback = (data) => {
    const { status, type } = data;
    const finishedStatuses = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setJDStep({ ...jDStep, run: false });
    }
  };
  //#endregion
  //#region config cho form ant
  const [formAdd] = Form.useForm();
  formAdd.setFieldsValue(
    globalConst.MODULE[config.module][config.constName].FORM?.ADD?.INITIAL,
  );
  const [formEdit] = Form.useForm();
  formAdd.setFieldsValue(
    globalConst.MODULE[config.module][config.constName].FORM?.EDIT?.INITIAL,
  );
  const [formReject] = Form.useForm();
  formAdd.setFieldsValue(
    globalConst.MODULE[config.module][config.constName].FORM?.REJECT?.INITIAL,
  );
  const formItemLayout = {
    className: "cs-ant-formitem",
    ...globalConst.ANT.FORM.ITEM.PARSER.TEXT_TRIMSTART, //trim đầu chuỗi tất cả input nhập
  };
  //#endregion
  //#region thông báo, tin nhắn
  notification.config({
    //set config ant notification
    placement: "bottomRight",
    maxCount: 3,
  });
  message.config({
    maxCount: 3,
  });
  //#endregion
  //#region pageNum pageSize
  const [pageNum, setPageNum] = useState(1);
  const [pageSize, setPageSize] = useState(20);
  //#endregion
  //#region data and inquiry
  const [inquirySet, setInquirySet] = useState({
    //request để gửi api truy vấn
    code: globalConst.MODULE[config.module][config.constName].SEARCH.SEARCHCODE,
    conds: [],
    sortConds: [],
    pageNum: pageNum,
    pageSize: pageSize,
    IsExport: 0,
    FileType: "excel",
    Language: lang,
  });

  const [dataSet, setDataSet] = useState({
    // dataset là res trả về từ api truy vấn
    recordCount: 0,
    pageCount: 0, // Số trang
    pageNum: pageNum, // trang hiện tại
    pageSize: pageSize, // số dòng mỗi trang
    datas: [],
  });
  //#endregion
  //#region truy vấn điều kiện
  // state filter
  const [filterSet, setFilterSet] = useState([]);
  // state selection
  const [selectedRowKeys, setSelectedRowKeys] = useState([1, 2]);
  const onSelectChange = (newSelectedRowKeys) => {
    setSelectedRowKeys([newSelectedRowKeys[newSelectedRowKeys.length - 1]]); // chọn phần tử cuối vì chỉ chọn một
  };
  const rowSelection = {
    type: "checkbox",
    fixed: true,
    hideSelectAll: true,
    selectedRowKeys,
    onChange: onSelectChange,
  };
  // set mặc định tiêu chí là tiêu chí đầu trong danh sách
  const [filter, setFilter] = useState(inquiry?.filterList[0]);
  const [filterOperator, setFilterOperator] = useState(
    inquiry?.filterList[0]?.operators[0],
  );
  const [filterKeySearch, setFilterKeySearch] = useState("");
  const [filterKeySearchText, setFilterKeySearchText] = useState("");

  // thêm filter vào ô
  const filterPush = () => {
    if (filter && filterOperator && filterKeySearch) {
      // push điều kiện lọc rồi set
      let thumbFilterSet = filterSet.filter((e) => e.key != filter.key);
      thumbFilterSet.push({
        checked: true,
        key: filter.key,
        keyText: filter.title,
        condition: filterOperator,
        keySearch: filterKeySearch,
        keySearchText: filterKeySearchText,
        show: `${
          lang == "vi" ? filter.title : filter.titleEn
        } ${filterOperator} "${filterKeySearchText}"`,
        query: `${filter.key} ${filterOperator} '${filterKeySearch}'`,
        isShow: true,
      });
      setFilterSet([...thumbFilterSet]);
    }
  };
  // toggle trạng thái checked của filter set
  const filterSetChoosenPush = (e, i) => {
    let thumbFilterSet = filterSet;
    if (e.target.checked) {
      thumbFilterSet[i].checked = true;
      setFilterSet([...thumbFilterSet]);
    } else {
      thumbFilterSet[i].checked = false;
      setFilterSet([...thumbFilterSet]);
    }
  };
  // xoá một filter set bằng dấu x bên cạnh
  const filterSetRemove = (index) => {
    // remove phần tử có index
    let thumbFilterSet = filterSet;
    thumbFilterSet.splice(index, 1);
    setFilterSet([...thumbFilterSet]);
  };
  // xoá nhiều filter set đã chọn
  const filterSetChoosenRemove = () => {
    let indexes = filterSet
      .map((e, i) => {
        if (e.checked === true) return i;
      })
      .filter((e) => e >= 0);
    // remove phần tử có index
    let thumbFilterSet = filterSet;
    indexes.forEach((index, index2) => {
      thumbFilterSet.splice(index - index2, 1);
    });
    // map ra obj lọc để gửi api
    setFilterSet([...thumbFilterSet]);
  };
  // uncheck tất cả filter set
  const filterSetUncheckAll = () => {
    let thumbFilterSet = filterSet;
    thumbFilterSet = thumbFilterSet.map((e) => {
      return {
        ...e,
        checked: false,
      };
    });
    setFilterSet([...thumbFilterSet]);
  };
  const unSelectRows = () => {
    setSelectedRowKeys([...[]]);
  };
  //#endregion

  const [tableFocused, setTableFocused] = useState(false);
  const [hiddenExpandButton, setHiddenExpandButton] = useState(false);

  //#region tự động làm mới
  const [isDataSetLoading, setIsDataSetLoading] = useState(false);
  const [isInquirySetLoading, setIsInquirySetLoading] = useState(false);

  const [fetchAgain, setFetchAgain] = useState(false); // có tự động fetch dữ liệu không
  const [fetchInterval, setFetchInterval] = useState(20); // tự động fetch dữ liệu sau x giây
  //#endregion

  //set columns lấy từ storage hoặc db
  const storageColumnsSaved = JSON.parse(
    localStorage.getItem("columnsLayouts") ?? "[]",
  );

  const [columnSet, setColumnSet] = useState([]);
  const [dragProps, setDragProps] = useState({});

  //
  const [searchMode, setSearchMode] = useState(false);
  // state popup modal
  const [showAddModal, setShowAddModal] = useState(false);
  const [showDetailsModal, setShowDetailsModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  // state add / edit
  const [dpChoosen, SetDpChoosen] = useState({});

  // fetch api với điều kiện tìm kiếm mặc định
  const fetchInquiryWithDefaultConds = () => {
    // lấy các điều kiện tìm kiếm mặc định
    let filterSetInitList = inquiry.filterList.filter(
      (e) => e.defValues.length > 0,
    );
    if (filterSetInitList.length > 0) {
      // có điều kiện tìm kiếm mặc định
      let flSetInit = filterSetInitList?.map((e) => {
        let keySearchText = e.options
          ?.filter((f) => e.defValues?.includes(f.cdVal))
          ?.map((f) => {
            return f.content;
          })
          ?.toString();

        return {
          checked: true,
          key: e.key,
          keyText: e.title,
          condition: e.operators[0],
          keySearch: e.defValues?.toString(),
          keySearchText: keySearchText,
          show: `${e.title} ${e.operators[0]} "${keySearchText}"`,
          query: `${e.key} ${e.operators[0]} '${e.defValues?.toString()}'`,
          isShow: false,
        };
      });
      setFilterSet([...flSetInit]);
      let q = {
        ...inquirySet,
        pageNum: 1,
        conds: [
          ...flSetInit
            .filter((e) => e.checked === true)
            .map((e) => {
              return {
                key: e.key,
                value: e.keySearch,
                operator: e.condition,
              };
            }),
        ],
      };
      setInquirySet({ ...q });
    }
  };
  //fetch api lấy danh sách dữ liệu
  const fetchInquiry = (inquiryReq = inquirySet) => {
    let controller = config;
    setIsDataSetLoading(!inquiryReq.IsExport);
    // gọi api
    Axios.inquiry(inquiryReq)
      .then((res) => {
        setIsDataSetLoading(false);
        if (res.status === 200) {
          if (inquiryReq.IsExport) {
            //chế độ kết xuất
            const a = document.createElement("a");
            const blob = new Blob([res.data], {
              type: "",
            });
            const url = window.URL.createObjectURL(blob);
            a.href = url;

            let headerLine = res.headers["content-disposition"];
            let filename = headerLine?.split("filename=")?.[1]?.split(";")?.[0];
            console.log(filename);

            a.download = filename;
            a.click();
            window.URL.revokeObjectURL(url);
          } else {
            //không kết xuất
            let data = {
              ...res.data,
              datas: [
                ...res.data.datas.map((e, i) => {
                  let thisRecordKey = "";
                  inquiry.recordKeys.map((k) => {
                    thisRecordKey += e[k] + "|";
                  });
                  thisRecordKey += i;

                  return {
                    ...e,
                    key: thisRecordKey,
                  };
                }),
              ],
            };
            setDataSet({ ...data });
            // console.log(inquirySet);
            // console.log(res.data.pageNum);
            // console.log(res.data.pageSize);
            // setInquirySet({
            //   ...inquirySet,
            //   pageNum: res.data.pageNum,
            //   pageSize: res.data.pageSize,
            // });

            if (res.data.recordCount === 0) {
              // notification.error({
              //   message: `Thất bại`,
              //   description: `Không tìm thấy dữ liệu theo điều kiện`,
              // });
            }
          }
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        setIsDataSetLoading(false);
        console.log(err);
        notification.error({
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
    return () => {};
  };
  // hande inquiry
  const handleInquiry = () => {
    // let q = {
    //   ...inquirySet,
    //   pageNum: 1,
    //   conds: [
    //     ...filterSet
    //       .filter((e) => e.checked === true)
    //       .map((e) => {
    //         return { key: e.key, value: e.keySearch, operator: e.condition };
    //       }),
    //   ],
    // };
    // if (filterSet.length === 0) fetchInquiryWithDefaultConds();
    // else setInquirySet(q);

    setInquirySet({
      ...inquirySet,
      pageNum: 1,
      conds: [
        ...filterSet
          .filter((e) => e.checked === true)
          .map((e) => {
            return { key: e.key, value: e.keySearch, operator: e.condition };
          }),
      ],
    });
  };
  // handle bấm sắp xếp trên cột
  const handleSortColumns = (sorter) => {
    let sortArr = [];
    if (Array.isArray(sorter)) sortArr = sorter;
    else sortArr.push(sorter);

    sortArr = sortArr
      .filter((e) => e.order != undefined)
      .map((e) => {
        return {
          Key: e.columnKey,
          Direction: e.order === "ascend" ? "asc" : "desc",
        };
      });

    setInquirySet({
      ...inquirySet,
      sortConds: [...sortArr],
    });
  };
  //#region Handle thêm sửa xoá duyệt từ chối
  // hande add single
  const handleAdd = (values) => {
    //gom dữ liệu gửi đi
    let objData = {
      ...values,
      //

      //
    };
    // gọi api
    Axios.addOne(objData)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.code > 0) {
            notification.success({
              message: `${t("thanhCong")}`,
              description: `${t("them")} ${
                globalConst.MODULE[config.module][config.constName].CATEGORY_VN
              } ${t("thanhCong")}`,
            });
            setShowAddModal(false);
            formAdd.resetFields(); //reset form nhập thêm
            unSelectRows();
          } else
            notification.error({
              message: `${t("thatBai")}`,
              description: `${res.data.code}: ${res.data.message}`,
            });
          fetchInquiry();
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          message: `${t("thatBai")}`,
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
  };
  // hande edit single
  const handleEdit = (values) => {
    let objData = {
      ...dpChoosen,
      ...values,
      //
      // LastChangeBy: "anonymous",
      // LastChangeDate: new Date().toISOString(),
      //
    };
    Axios.updateOne(objData)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.code > 0) {
            notification.success({
              message: `${t("thanhCong")}`,
              description: `${t("sua")} ${
                globalConst.MODULE[config.module][config.constName].CATEGORY_VN
              } ${t("thanhCong")}`,
            });
            setShowEditModal(false);
            unSelectRows();
            formEdit.resetFields(); // reset form nhập sửa
          } else
            notification.error({
              message: `${t("thatBai")}`,
              description: `${res.data.code}: ${res.data.message}`,
            });
          fetchInquiry();
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          message: `${t("thatBai")}`,
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
  };
  const handleActive = (checked, code) => {
    let objData = {
      code: code,
      active: checked,
      //
      LastChangeBy: "anonymous",
      LastChangeDate: new Date().toISOString(),
      //
    };
    Axios.activeOne(objData)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.code > 0) {
            notification.success({
              message: `${t("thanhCong")}`,
              description: `${t("active")} ${
                globalConst.MODULE[config.module][config.constName].CATEGORY_VN
              } ${t("thanhCong")}`,
            });
            unSelectRows();
          } else
            notification.error({
              message: `${t("thatBai")}`,
              description: `${res.data.code}: ${res.data.message}`,
            });
          fetchInquiry();
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          message: `${t("thatBai")}`,
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
  };
  // hande delete single
  const handleDelete = () => {
    let objData = {
      id: dpChoosen?.id,
      code: dpChoosen?.code,
      //
      LastChangeBy: "anonymous",
      LastChangeDate: new Date().toISOString(),
      //
    };
    Axios.deleteOne(objData)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.code > 0) {
            notification.success({
              message: `${t("thanhCong")}`,
              description: `${t("xoaBanGhiThanhCong")}`,
            });
            setShowDeleteModal(false);
            unSelectRows();
          } else
            notification.error({
              message: `${t("thatBai")}`,
              description: `${res.data.code}: ${res.data.message}`,
            });
          fetchInquiry();
        } else {
          console.log(res);
          notification.error({
            message: `${t("thatBai")}`,
            description: `${t("loiKetNoiHeThong")}`,
          });
        }
      })
      .catch(function (err) {
        console.log(err);
        notification.error({
          message: `${t("thatBai")}i`,
          description: `${t("loiKetNoiHeThong")}`,
        });
      });
  };
  //#endregion

  //#region Export
  const handleExport = (filetype) => {
    setTableFocused(false);
    fetchInquiry({
      ...inquirySet,
      IsExport: 1,
      FileType: filetype,
      Language: lang,
    });
  };
  //#endregion

  //#region useEffect
  useEffect(() => {
    SetDpChoosen({
      ...dataSet.datas.find((e) => selectedRowKeys.includes(e.key)),
    });
  }, [selectedRowKeys]);
  // Xử lý render dữ liệu sau khi lấy được các config truy vấn từ server
  useEffect(() => {
    // map lại columns theo thằng ant
    if (inquiry.columnList.length > 0) {
      let columnsFromStore = storageColumnsSaved[CONTROLLER];

      let thumbInquiryColumns =
        columnsFromStore && columnsFromStore.length > 0
          ? inquiry.columnList.sort((a, b) => {
              if (
                columnsFromStore.indexOf(a.key) <
                columnsFromStore.indexOf(b.key)
              ) {
                return -1;
              }
              if (
                columnsFromStore.indexOf(a.key) >
                columnsFromStore.indexOf(b.key)
              ) {
                return 1;
              }
              return 0;
            })
          : inquiry.columnList;

      if (columnSet?.length === 0) {
        //set columns từ db
        setColumnSet([
          ...MapColumns(
            thumbInquiryColumns.map((e) => {
              return {
                ...e,
                title: lang === "en" ? e.titleEn : e.title,
                fixed:
                  e.fixed === "L" ? "left" : e.fixed === "R" ? "right" : "",
                align:
                  e.align === "C"
                    ? "center"
                    : e.align === "R"
                    ? "right"
                    : "left",
                sorter: e.sortable === "Y" ? { multiple: 1 } : false,
              };
            }),
            handleActive,
          ),
        ]);
      }
    }

    return () => {};
  }, [inquiry]);

  useEffect(() => {
    setDragProps({
      ...{
        onDragEnd(fromIndex, toIndex) {
          fromIndex = fromIndex - 1;
          toIndex = toIndex - 1;

          if (
            columnSet[fromIndex]?.fixed ||
            columnSet[toIndex]?.fixed ||
            toIndex === -1
          ) {
            return message.error(`${t("koTheKeoVaoViTriNay")}`);
          }

          const columns = [...columnSet];
          const item = columns.splice(fromIndex, 1)[0];
          columns.splice(toIndex, 0, item);
          //lưu storage
          //Lấy columns đã save, nếu ko tìm thấy key của chức năng này thì push vào mảng, còn nếu có thì storageColumnsSaved[i] =
          let current_columnsLayouts =
            JSON.parse(localStorage.getItem("columnsLayouts")) ?? {};

          current_columnsLayouts[CONTROLLER] = columns.map((e, i) => e.key);

          localStorage.setItem(
            "columnsLayouts",
            JSON.stringify(current_columnsLayouts),
          );

          setColumnSet([...columns]);
        },
        nodeSelector: "th",
      },
    });
    return () => {};
  }, [columnSet]);

  useEffect(() => {
    console.log("Bản ghi đã chọn: ", dpChoosen);
    formEdit.resetFields();
    formEdit.setFieldsValue({
      ...dpChoosen,
      ...globalConst.MODULE[config.module][config.constName].FORM.EDIT.INITIAL,
    });

    formReject.setFieldsValue({ AUTOID: dpChoosen?.AUTOID });
    return () => {};
  }, [dpChoosen]);

  useEffect(() => {
    fetchInquiry(); //fetch truy vấn
  }, [
    // inquiry,
    inquirySet.pageNum,
    inquirySet.pageSize,
    inquirySet.sortConds,
    inquirySet.conds,
    fetchAgain,
    fetchInterval,
  ]);

  useEffect(() => {
    if (fetchAgain) {
      var autoCall = setInterval(() => {
        fetchInquiry();
      }, fetchInterval * 1000);

      return function cleanup() {
        clearInterval(autoCall);
      };
    }
  }, [dataSet, fetchAgain, fetchInterval]);
  //#endregion

  return (
    <>
      <Joyride
        callback={handleJoyrideCallback}
        continuous
        // hideBackButton
        hideCloseButton
        run={jDStep.run}
        scrollToFirstStep
        showProgress
        showSkipButton
        disableScrolling
        // disableOverlay
        steps={jDStep.steps}
        styles={{
          options: {
            zIndex: 10000,
          },
          buttonClose: {
            height: 14,
            padding: 15,
            position: "absolute",
            right: 0,
            background: "none",
            top: 0,
            width: 14,
          },
          buttonNext: {
            background: "#4086E7",
            borderRadius: "8px",
          },
          buttonSkip: {
            background: "rgb(0 0 0)",
            borderRadius: "8px",
          },
          buttonLast: {
            background: "#4086E7",
            borderRadius: "8px",
          },
          buttonBack: {
            background: "rgb(138 138 138)",
            borderRadius: "8px",
          },
        }}
      />

      <div className="main">
        {/* HTML block seach */}
        <div className="block-search fix-header">
          <Affix
            offsetTop={100}
            onChange={(affixed) => setHiddenExpandButton(affixed)}
          >
            <div
              className="container"
              style={{
                zIndex: 1051,
                background: "white",
              }}
            >
              <div className="head-search">
                <div className="title">
                  {/* {t("quanLy")} {globalConst.MODULE[config.module][config.constName].CATEGORY_VN} */}
                  <span>{lang == "en" ? inquiry.titleEn : inquiry.title}</span>
                  <span
                    className="ml-2"
                    style={{
                      fontSize: ".9rem",
                      color: "purple",
                    }}
                  >
                    [{inquiry.keyWord ?? "000000"}]
                  </span>

                  {/* Nút bắt đấu hướng dẫn  */}
                  <span
                    className="ml-2"
                    onClick={() => {
                      setJDStep({ ...jDStep, run: true });
                    }}
                  >
                    <FontAwesomeIcon icon="fa-solid fa-circle-question" />
                  </span>
                </div>
                <div className="d-flex align-items-center records-actions">
                  <div className="block-utilities mr-5 pr-5"></div>
                  <div className="search-right">
                    <div className="block-utilities">
                      {/* Chức năng thêm mới  */}
                      {!globalConst.MODULE[config.module][config.constName].FORM
                        ?.ADD?.DISABLED && (
                        <FormMaster
                          formItemLayout={formItemLayout}
                          formInstance={formAdd}
                          handleAction={handleAdd}
                          dpChoosen={dpChoosen}
                          inquiry={inquiry}
                          showModal={showAddModal}
                          setShowModal={setShowAddModal} //
                          config={config}
                          action={`${t("them")}`}
                          icon={`/image/icon/ic-add.svg`}
                          clearFormValueOnClose
                          fullscreen={
                            globalConst.MODULE[config.module][config.constName]
                              .FORM?.LAYOUT?.FULLSCREEN
                          }
                          InputItems={config.InputItems}
                        />
                      )}

                      {/* Chức năng xem chi tiết  */}
                      {!globalConst.MODULE[config.module][config.constName].FORM
                        ?.DETAIL?.DISABLED && (
                        <FormMaster
                          formItemLayout={formItemLayout}
                          formInstance={formEdit}
                          dpChoosen={dpChoosen}
                          inquiry={inquiry}
                          showModal={showDetailsModal}
                          setShowModal={setShowDetailsModal} //
                          config={config}
                          action={`${t("xemChiTiet")}`}
                          icon={`/image/icon/ic-details.svg`}
                          requireDpChoosen
                          disabledForm
                          fullscreen={
                            globalConst.MODULE[config.module][config.constName]
                              .FORM?.LAYOUT?.FULLSCREEN
                          }
                          InputItems={config.InputItems}
                        />
                      )}
                      {/* Chức năng sửa  */}
                      {!globalConst.MODULE[config.module][config.constName].FORM
                        ?.EDIT?.DISABLED && (
                        <FormMaster
                          formItemLayout={formItemLayout}
                          formInstance={formEdit}
                          handleAction={handleEdit}
                          dpChoosen={dpChoosen}
                          inquiry={inquiry}
                          showModal={showEditModal}
                          setShowModal={setShowEditModal} //
                          config={config}
                          action={`${t("sua")}`}
                          icon={`/image/icon/ic-update.svg`}
                          requireDpChoosen
                          fullscreen={
                            globalConst.MODULE[config.module][config.constName]
                              .FORM?.LAYOUT?.FULLSCREEN
                          }
                          InputItems={config.InputItems}
                        />
                      )}
                      {/* Nút xoá  */}
                      {!globalConst.MODULE[config.module][config.constName].FORM
                        ?.DELETE?.DISABLED && (
                        <>
                          <button
                            onClick={() => {
                              if (!dpChoosen?.key)
                                return message.error(
                                  `${t("koCoBanGhiNaoDcChon")}`,
                                );
                              setShowDeleteModal(true);
                            }}
                          >
                            <img src="/image/icon/ic-delete.svg" alt="" />
                            <span>{t("xoa")}</span>
                          </button>
                          <Modal
                            centered
                            className="cs-ant-modal cs-ant-modal__confirm_popup"
                            destroyOnClose={true}
                            visible={showDeleteModal}
                            title={t("thongBaoChapNhan")}
                            onOk={() => {
                              setShowDeleteModal(false);
                            }}
                            onCancel={() => {
                              setShowDeleteModal(false);
                            }}
                            footer={[
                              <button
                                className="btn-cancel"
                                onClick={() => {
                                  setShowDeleteModal(false);
                                }}
                              >
                                <img
                                  src="/image/icon/ic-cancel.svg"
                                  alt="img"
                                />
                                {t("huy")}
                              </button>,
                              <Button
                                type="primary"
                                htmlType="submit"
                                className="btn-confirm p-0"
                                onClick={() => {
                                  handleDelete();
                                  setShowDeleteModal(false);
                                }}
                              >
                                <img
                                  src="/image/icon/ic-confirm.svg"
                                  alt="img"
                                />
                                {t("chapNhan")}
                              </Button>,
                            ]}
                          >
                            <span className="confirm_popup__content">
                              {`${t("banCoChacMuonXoaBanGhiNay")}`}
                            </span>
                          </Modal>
                        </>
                      )}
                      <Dropdown
                        onVisibleChange={(visible) => {
                          setTableFocused(visible);
                        }}
                        overlay={
                          <Menu
                            items={[
                              {
                                label: (
                                  // <Spin>
                                  <div onClick={() => handleExport("excel")}>
                                    <FontAwesomeIcon
                                      color="#00a400"
                                      size="lg"
                                      icon="fa-solid fa-file-excel"
                                    />
                                    <span className="pl-2">Excel</span>
                                  </div>
                                  // </Spin>
                                ),
                                key: "0",
                              },
                              // {
                              //   label: (
                              //     <div onClick={() => handleExport("pdf")}>
                              //       <FontAwesomeIcon
                              //         color="#3663c1"
                              //         icon="fa-solid fa-file-word"
                              //       />
                              //       <span className="pl-2">Word</span>
                              //     </div>
                              //   ),
                              //   key: "1",
                              // },
                              {
                                label: (
                                  <div onClick={() => handleExport("pdf")}>
                                    <FontAwesomeIcon
                                      color="#ff0101"
                                      size="lg"
                                      icon="fa-solid fa-file-pdf"
                                    />
                                    <span className="pl-2">PDF</span>
                                  </div>
                                ),
                                key: "1",
                              },
                              {
                                type: "divider",
                              },
                            ]}
                          />
                        }
                        trigger={["click"]}
                      >
                        <button>
                          <img src="/image/icon/ic-export.svg" alt="" />
                          <span>{t("ketXuat")}</span>
                        </button>
                      </Dropdown>
                    </div>
                    <button
                      className="btn-extend"
                      id="btn-extend"
                      style={{
                        opacity: hiddenExpandButton ? "0" : "1",
                      }}
                    >
                      <div className="btn1">
                        <FontAwesomeIcon icon="fa-solid fa-chevron-up" />
                        <span>{t("thuGon")}</span>
                      </div>
                      <div className="step step3">
                        <FontAwesomeIcon icon="fa-solid fa-chevron-down" />
                        <span>{t("moRong")}</span>
                      </div>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </Affix>
        </div>
        {!globalConst.MODULE[config.module][config.constName].SEARCH?.DISABLED && (
          <>
            {/* Tìm kiếm mở rộng */}
            <div className="step step1">
              <div className="search">
                <div className="container">
                  <div className="search__wrap">
                    <InquiryForm
                      inquiry={inquiry}
                      filter={filter}
                      filterOperator={filterOperator}
                      setFilter={setFilter}
                      setFilterKeySearch={setFilterKeySearch}
                      setFilterKeySearchText={setFilterKeySearchText}
                      setFilterOperator={setFilterOperator}
                    />
                    <div>
                      <button
                        className="btn-condition mb-3"
                        onClick={filterPush}
                      >
                        {t("themDieuKien")}
                        <img
                          src="/image/icon/ic_add_dk.svg"
                          alt=""
                          className="mr-0"
                        />
                      </button>
                      <button
                        className="btn-condition"
                        onClick={filterSetChoosenRemove}
                      >
                        <img
                          src="/image/icon/ic_remove_dk.svg"
                          alt=""
                          className="ml-0"
                        />
                        {t("xoaDieuKien")}
                      </button>
                    </div>
                    <div className="search__list">
                      <div className="search__list-name">
                        {t("danhSachDieuKienTimKiem")}
                      </div>
                      <ul className="scroll-bar">
                        {filterSet.map(
                          (e, i) =>
                            e.isShow && (
                              <li
                                key={JSON.stringify(e) + i}
                                className="search__list-item"
                              >
                                <input
                                  checked={e.checked}
                                  onChange={(ele) =>
                                    filterSetChoosenPush(ele, i)
                                  }
                                  type="checkbox"
                                />
                                <div>{e.show}</div>
                                <a onClick={() => filterSetRemove(i)}>
                                  <img src="/image/icon/ic-remove.svg" alt="" />
                                </a>
                              </li>
                            ),
                        )}
                      </ul>
                    </div>
                    <div className="d-flex flex-column align-items-center ml-4">
                      <button className="btn-search" onClick={handleInquiry}>
                        <img src="/image/icon/ic-search-white.svg" alt="" />
                        {t("timKiem")}
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Tìm kiếm thu nhỏ */}
            <div className="step step2">
              <div className="search">
                <div className="container">
                  {filterSet.length > 0 && (
                    <div className="search__wrap short">
                      <div className="search__short">
                        {filterSet.map(
                          (e, i) =>
                            e.isShow && (
                              <div
                                key={"search-short" + JSON.stringify(e) + i}
                                className="search__short-item"
                              >
                                <input
                                  type="checkbox"
                                  checked={e.checked}
                                  onChange={(ele) =>
                                    filterSetChoosenPush(ele, i)
                                  }
                                />
                                <div>{e.show}</div>
                              </div>
                            ),
                        )}
                        <a
                          href="javascrip:0;"
                          onClick={filterSetUncheckAll}
                        ></a>
                      </div>

                      <button className="btn-search" onClick={handleInquiry}>
                        <img src="/image/icon/ic-search-white.svg" alt="" />
                        {t("timKiem")}
                      </button>
                    </div>
                  )}
                </div>
              </div>
            </div>
            {/* End */}
          </>
        )}
        {/* Overlay focus table */}
        <div
          onClick={() => {
            setTableFocused(false);
          }}
          style={{
            display: tableFocused ? "block" : "none",
            width: "100vw",
            height: "-webkit-fill-available",
            position: "absolute",
            top: "0",
            background: "rgb(0 0 0 / 15%)",
          }}
        ></div>
        {/* End */}
        {/* HTML block record */}
        <div className="block-record">
          {/* Xem chi tiết cũ  */}
          {/* <Drawer
            className="cs-ant-drawer"
            title={`Chi tiết ${globalConst.MODULE[config.module][config.constName].CATEGORY_VN}`}
            placement="right"
            closable={false}
            width={660}
            onClose={() => {
              setShowDetailsModal(false);
            }}
            visible={showDetailsModal}
            getContainer={false}
            style={{ position: "fixed" }}
          >
            <DetailsView record={dpChoosen} inquiry={inquiry} />
          </Drawer> */}
          {/* Icon loading dữ liệu  */}
          <Spin spinning={isDataSetLoading}>
            <div className="container">
              <ReactDragListView.DragColumn {...dragProps}>
                <Table
                  className={`over-flow-x table-auto-width cs-ant-table table-drag-column ${
                    tableFocused ? "tableFocused" : ""
                  }`}
                  columns={columnSet}
                  dataSource={[...dataSet.datas]}
                  size="small"
                  // tableLayout="fixed"
                  // locale={tableHelper.locale}
                  rowSelection={rowSelection}
                  onChange={(pagination, filters, sorter, extra) => {
                    handleSortColumns(sorter);
                  }}
                  onRow={(record, rowIndex) => {
                    return {
                      onClick: (event) => {
                        setSelectedRowKeys([record?.key]);
                        SetDpChoosen({
                          ...dpChoosen,
                          ...record,
                        });
                      }, // click row
                      onDoubleClick: (event) => {}, // double click row
                      onContextMenu: (event) => {}, // right button click row
                      onMouseEnter: (event) => {}, // mouse enter row
                      onMouseLeave: (event) => {}, // mouse leave row
                    };
                  }}
                  pagination={{
                    current: inquirySet.pageNum,
                    pageSize: inquirySet.pageSize, //số dòng mỗi trang
                    position: ["none", "none"], //không hiển thị pagination
                  }}
                  scroll={{
                    y: 600,
                  }}
                ></Table>
              </ReactDragListView.DragColumn>
              <Pagination
                className="pagination-bar"
                currentPage={inquirySet.pageNum}
                totalCount={dataSet.recordCount}
                pageSize={inquirySet.pageSize}
                onPageChange={(page) => {
                  setInquirySet({ ...inquirySet, pageNum: parseInt(page) });
                }}
                onSizeChange={(size) => {
                  setInquirySet({
                    ...inquirySet,
                    pageSize: parseInt(size),
                    pageNum: 1,
                  });
                }}
                setFetchAgain={setFetchAgain}
                setFetchInterval={setFetchInterval}
              />
            </div>
          </Spin>
        </div>
        {/* End */}
      </div>
    </>
  );
};
