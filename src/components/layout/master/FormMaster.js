// Author : Can Xuan Tung – Navisoft.
import {
  Affix,
  Button,
  Form,
  Input,
  message,
  Modal,
  notification,
  Select,
  Space,
  Spin,
  Table,
  Tabs,
  DatePicker,
  InputNumber,
  Divider,
} from "antd";
import { useSelector } from "react-redux";
import { useGlobalConst } from "../../../helpers/constData";
import moment from "moment";
import { useEffect, useState } from "react";
import { t } from "i18next";

const { Option } = Select;
const FormMaster = ({
  formItemLayout,
  formInstance,
  handleAction,
  requireDpChoosen = false,
  dpChoosen,
  inquiry,
  showModal,
  setShowModal,
  config,
  action = "", //Thêm /sửa /xem chi tiết
  icon, //icon nút
  fullscreen = false,
  disabledForm = false,
  clearFormValueOnClose = false,
  InputItems,
}) => {
  const globalConst = useGlobalConst(t);
  const [formValues, setFormValues] = useState({});

  const handleChangeFormValues = (changedValues, allValues) => {
    console.log("form value changed: ");
    console.log(changedValues);
    console.log("form values: ");
    console.log(allValues);

    setFormValues(allValues);
  };
  const handleFinishForm = (values) => {
    console.log("finished form with values: ");
    console.log(values);
  };
  const hanldeCancelModel = () => {
    setShowModal(false);
    if (clearFormValueOnClose) formInstance.resetFields(); // reset form nhập
  };
  return (
    <>
      <button
        onClick={() => {
          if (requireDpChoosen && !dpChoosen?.key)
            // cho sửa/xem chi tiết
            return message.error(`${t("koCoBanGhiNaoDcChon")}`);
          if (
            action === t("sua") &&
            dpChoosen?.STATUS === globalConst.APP.CODE.STATUS.EXPIRED
          )
            return notification.error({
              message: `Thất bại`,
              description: `${t("banGhiDaHetHieuLuc")}`,
              placement: "topRight",
            });
          setShowModal(true);
        }}
      >
        <img src={icon} alt="" />
        <span>{action}</span>
      </button>
      <Modal
        centered
        className={`cs-ant-modal cs-ant-modal__spacing ${
          fullscreen ? "cs-ant-modal__fullscreen" : ""
        }`}
        destroyOnClose={true}
        visible={showModal}
        title={`${action} ${
          globalConst.MODULE[config.module][config.constName].CATEGORY_VN
        }`}
        onOk={() => {
          setShowModal(false);
        }}
        onCancel={hanldeCancelModel}
        footer={[
          <button
            className="btn-cancel"
            data-dismiss="modal"
            aria-label="Close"
            onClick={hanldeCancelModel}
          >
            <img src="/image/icon/ic-cancel.svg" alt="img" />
            {`${t("huy")}`}
          </button>,
          <Form.Item shouldUpdate className="submit mb-0">
            {() => (
              <Button
                type="primary"
                htmlType="submit"
                className="btn-confirm p-0"
                onClick={() => {
                  formInstance
                    .validateFields()
                    .then((values) => {
                      console.log("values ...", values);
                      formInstance.submit();
                      handleAction(values);
                    })
                    .catch((errorInfo) => {
                      console.log("errorInfo ...", errorInfo);
                    });
                }}
              >
                <img src="/image/icon/ic-confirm.svg" alt="img" />
                {`${t("chapNhan")}`}
              </Button>
            )}
          </Form.Item>,
        ]}
      >
        {/* <Tabs className="cs-ant-tabs" defaultActiveKey="1">
          <Tabs.TabPane
            className="cs-ant-tabpane"
            tab=""
            key="1"
          > */}
        <Form
          className={`cs-ant-form ${fullscreen ? "" : "mini-ant-form"}`}
          onFinish={handleFinishForm}
          onValuesChange={handleChangeFormValues}
          form={formInstance}
          name="formInstance"
          size={"large"}
          disabled={disabledForm}
        >
          <Form.Item name={"id"} hidden>
            <Input />
          </Form.Item>
          <InputItems
            formItemLayout={formItemLayout}
            inquiry={inquiry}
            formValues={formValues}
            isEditing={action === t("sua")}
          />
        </Form>
        {/*  */}
      </Modal>
    </>
  );
};
export default FormMaster;
