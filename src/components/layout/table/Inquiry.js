import { Badge, Dropdown, Menu, Space, Table, Tooltip } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { Select } from "antd";
import React, { useEffect, useState } from "react";
import { useGlobalConst } from "../../../helpers/constData";
import { t } from "i18next";
const { Option } = Select;

// inquiry
const InquiryForm = ({
  inquiry,
  filter,
  filterOperator,
  setFilterKeySearch,
  setFilterKeySearchText,
  setFilter,
  setFilterOperator,
}) => {
  const [cbmVal, setCbmVal] = useState([]);
  const [cbsVal, setCbsVal] = useState();
  const [txtVal, setTxtVal] = useState();

  return (
    <div className="search__option">
      <SearchField
        inquiry={inquiry}
        filter={filter}
        setFilter={setFilter}
        setFilterKeySearch={setFilterKeySearch}
        setFilterKeySearchText={setFilterKeySearchText}
        setFilterOperator={setFilterOperator}
        setCbmVal={setCbmVal}
        setCbsVal={setCbsVal}
        setTxtVal={setTxtVal}
      />
      <SearchOperators
        filter={filter}
        filterOperator={filterOperator}
        setFilterOperator={setFilterOperator}
      />
      <SearchControl
        cbmVal={cbmVal}
        cbsVal={cbsVal}
        txtVal={txtVal}
        filter={filter}
        setFilterKeySearch={setFilterKeySearch}
        setFilterKeySearchText={setFilterKeySearchText}
        setCbmVal={setCbmVal}
        setCbsVal={setCbsVal}
        setTxtVal={setTxtVal}
      />
    </div>
  );
};

//render field search
const SearchField = ({
  inquiry,
  filter,
  setFilter,
  setFilterKeySearch,
  setFilterOperator,
  setCbmVal,
  setCbsVal,
  setTxtVal,
}) => {
  const globalConst = useGlobalConst(t);
  const lang = useSelector((state) => state.language.language);
  return (
    <div className="search__option-box">
      <label>{t('tieuChi')}</label>
      <Select
        className="item-search sl bdr-bottom cs-ant-select"
        mode="single"
        allowClear
        placeholder={`${t('luaChon')}`}
        value={filter?.key}
        onChange={(value) => {

          let filterNew = inquiry.filterList?.find((e) => e.key === value);
          setFilter({ ...filterNew });

          setFilterKeySearch("");
          // clear các giá trị tìm kiếm mỗi lần thay đổi trường tìm kiếm
          setCbmVal([...[]]);
          setCbsVal(null);
          setTxtVal("");

          //lấy các contdion tìm kiếm LIKE = 
          setFilterOperator(filterNew.operators?.[0]);
        }}
      >
        {
        inquiry.filterList?.map((e) => (
          <Option key={"Fsield" + e.key} value={e.key}>
            {lang == 'vi' ? e.title : e.titleEn }
          </Option>
        ))}
      </Select>
    </div>
  );
};

//render field operators
const SearchOperators = ({ filter, setFilterOperator, filterOperator }) => {
  const globalConst = useGlobalConst(t);
  return (
    <div className="search__option-box">
      <label>{t('dieuKien')}</label>
      <Select
        className="item-search sl bdr-bottom cs-ant-select"
        mode="single"
        allowClear
        placeholder={`${t('luaChon')}`}
        value={filterOperator}
        onChange={(value) => {
          setFilterOperator(value);
        }}
      >
        {filter?.operators?.map((e) => (
          <Option key={"sOperator" + e} value={e}>
            {e}
          </Option>
        ))}
      </Select>
    </div>
  );
};

//render field search control
const SearchControl = ({
  cbsVal,
  cbmVal,
  txtVal,
  setCbmVal,
  setCbsVal,
  setTxtVal,
  filter,
  setFilterKeySearch,
  setFilterKeySearchText,
}) => {
  const globalConst = useGlobalConst(t);
  const lang = useSelector((state) => state.language.language);
  return (
    <div className="search__option-box bdr-bottom">
      <label>{t('giaTri')}</label>
      {filter?.control == globalConst.APP.FORM.TYPE.TEXTBOX && (
        <input
          onChange={(ele) => {
            setTxtVal(ele.target.value);
            setFilterKeySearch(ele.target.value);
            setFilterKeySearchText(ele.target.value);
          }}
          type="text"
          placeholder={t('nhapGiaTri')}
          value={txtVal}
        />
      )}
      {filter?.control ==
        globalConst.APP.FORM.TYPE.COMBOBOX_MULTI && (
        <Select
          className="item-search sl bdr-bottom cs-ant-select"
          mode="multiple"
          placeholder={t('luaChon')}
          value={cbmVal}
          onChange={(values) => {
            setCbmVal(values);
            setFilterKeySearch(values.toString());
            setFilterKeySearchText(
              filter?.options
                .filter((e) => values.includes(e.cdVal))
                .map((e) => lang == 'vi' ? e.content : e.contentEn)
                .toString(),
            );
          }}
        >
          {filter?.options?.map((e) => (
            <Option key={"sOption" + e.cdVal} value={e.cdVal}>
              {lang == 'vi' ? e.content : e.contentEn}
            </Option>
          ))}
        </Select>
      )}
      {filter?.control ==
        globalConst.APP.FORM.TYPE.COMBOBOX_SINGLE && (
        <Select
          className="item-search sl bdr-bottom cs-ant-select"
          mode="single"
          placeholder={`${t('luaChon')}`}
          value={cbsVal}
          onChange={(value) => {
            setCbsVal(value);
            setFilterKeySearch(value);
            setFilterKeySearchText(
              filter.options.find((e) => value == e.cdVal)?.content,
            );
          }}
        >
          {filter?.options?.map((e) => (
            <Option key={"sOption" + e.cdVal} value={e.cdVal}>
              {e.content}
            </Option>
          ))}
        </Select>
      )}
    </div>
  );
};

export default InquiryForm;
