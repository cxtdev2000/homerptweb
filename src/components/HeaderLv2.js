// Author : Can Xuan Tung – Navisoft.
import { useTranslation } from "react-i18next";
import { DatePicker, Space } from "antd";
import { Select } from "antd";

const { Option } = Select;

export const HeaderLv2 = (props) => {
  const { t } = useTranslation();

  return (
    <div className="main bg-gray pt-60">
        <div className="block-info">
          <div className="box-info">
            <div className="head-info">
              Lưu ký chứng khoán
            </div>
            <div className="body-info">
              <a href="javascript:0;">2240: Lưu ký chứng khoán</a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">2246: Hoàn tất lưu ký chứng khoán</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
            </div>
          </div>
          <div className="box-info">
            <div className="head-info">
              Chuyển khoản CK
            </div>
            <div className="body-info">
              <a href="javascript:0;">2240: Lưu ký chứng khoán</a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">2246: Hoàn tất lưu ký chứng khoán</a>
              <a href="javascript:0;">Hoàn tất chuyển khoản ra ngoài </a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">2246: Hoàn tất lưu ký chứng khoán</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
              <a href="javascript:0;">Hủy chuyển khoản ra ngoài</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
            </div>
          </div>
          <div className="box-info">
            <div className="head-info">
              Phong tỏa CK
            </div>
            <div className="body-info">
              <a href="javascript:0;">2240: Lưu ký chứng khoán</a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">2246: Hoàn tất lưu ký chứng khoán</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
            </div>
          </div>
          <div className="box-info">
            <div className="head-info">
              Cầm cố CK
            </div>
            <div className="body-info">
              <a href="javascript:0;">2240: Lưu ký chứng khoán</a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">2246: Hoàn tất lưu ký chứng khoán</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
              <a href="javascript:0;">2240: Lưu ký chứng khoán</a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">2246: Hoàn tất lưu ký chứng khoán</a>
              <a href="javascript:0;">2254: Hủy lưu ký chứng khoán</a>
            </div>
          </div>
          <div className="box-info">
            <div className="head-info">
              Import giao dịch theo file
            </div>
            <div className="body-info">
              <a href="javascript:0;">Import GD gửi lưu lý CK</a>
              <a href="javascript:0;">Import GD chuyển khoản CK ra ngoài</a>
              <a href="javascript:0;">Import GD nhận chuyển khoản CK từ ngoài vào</a>
              <a href="javascript:0;">Import GD chuyển khoản CK nội bộ</a>
            </div>
          </div>
          <div className="box-info">
            <div className="head-info">
              QL KH thuộc TVLK khác
            </div>
            <div className="body-info">
              <a href="javascript:0;">Import số dư tiền</a>
              <a href="javascript:0;">Import số dư chứng khoán</a>
            </div>
          </div>
          <div className="box-info">
            <div className="head-info">
              Tra cứu 
            </div>
            <div className="body-info">
              <a href="javascript:0;">2240: Lưu ký chứng khoán</a>
              <a href="javascript:0;">2241: Tái lưu ký chứng khoán lên TTLK</a>
              <a href="javascript:0;">Tra cứu số dư chứng khoán trong quá khứ</a>
              <a href="javascript:0;">Hủy lưu ký chứng khoán</a>
              <a href="javascript:0;">Tra cứu giao dịch lưu ký</a>
            </div>
          </div>
        </div>
      </div>
  );
};
