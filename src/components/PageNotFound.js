
import React from 'react';
import { useTranslation } from 'react-i18next';


function PageNotFound() {
    const { t } = useTranslation();
    return (
        <div style={{color:"#fff",textAlign:"center"}}>
            {t("khongTimThayTrangYeuCau")}
        </div>
    );
}

export default PageNotFound;