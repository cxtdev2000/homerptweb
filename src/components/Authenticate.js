
/*
- Khi vào page thì ứng dụng sẽ qua đây để xác thực
*/

import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";

function Authenticate() {
    // mục đích lấy chuỗi data từ querystring
    const queryParams = new URLSearchParams(window.location.search);
    const getDataEncrypt = queryParams.get("data");
    //
    const[dataQuery,setDataQuery]=useState("");
    
    let history = useHistory();

    useEffect(()=> {
        // 1. nếu đã có thông tin trong localStore thì thôi k cần check => 
        // điều hướng thẳng vào màn hình đã đăng nhập
        // -------------------------------------------------//
        // 2. nếu chưa có thì gửi thông tin chuỗi mã hóa sang API để giải mã
        console.log('1.Send API',getDataEncrypt);
        // 2. lấy được thông tin trả về từ api lưu vào localstore
        // 3. điều hướng tương ứng với màn hình
        history.push("/banggia")
    },[]);

    return (
        <div> 
            {console.log('2.render--xxxxx',getDataEncrypt)}
            {getDataEncrypt}
        </div>
    );    
}

export default Authenticate;