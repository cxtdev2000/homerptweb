import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function TestNotify() {
    const notifySuccess = () => toast.success("Success.");
    const notifyError = () => toast.error("Error.");
    const notifyInfo=()=>toast.info("Info.")

    return (
        <div>
            <button onClick={notifySuccess}>Success</button>
            <button onClick={notifyError}>Error</button>
            <button onClick={notifyInfo}>Info</button>
            
            <ToastContainer position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover />
        </div>
    );
}

export default TestNotify;