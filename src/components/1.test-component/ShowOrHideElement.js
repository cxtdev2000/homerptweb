
import React,{useState} from 'react';

import {ReadNumber} from '../../helpers/utils'

function ShowOrHideElement() {
    const [showText,setShowText] = useState(false);

    const handleSetShowText = () => setShowText(true);

    const [textValue,setTextValue] = useState('');

    const handleChangeOption = (_val)=>{
        console.log('Bạn vừa chọn giá trị',_val);
        if(_val===null || _val==='')
            setTextValue('');
        else
        setTextValue("=====> " + _val);
    };

    return (
        <div>
            <button onClick={handleSetShowText}>Click Button</button>   
            {ReadNumber(-100000)}
            <select onChange={(e)=>handleChangeOption(e.target.value)}>
                <option value=""></option>
                <option value="1">Có</option>
                <option value="2">Không</option>
            </select>
            <br/>
            {
                (textValue!== "" || textValue!=="") ? <><span id="spGetText">Test {textValue}</span></>:""
            }         
        </div>
    );
}

export default ShowOrHideElement;