import React,{useMemo,useState,useEffect} from 'react';

function Test_useMemo() {

    const [count,setCount] = useState(0);

    const Crease = () => {
        let num = count;
        setCount(++num);
    };

    const findStudent = () =>{
        let arrStudent = [];
        let itemSpecify;

        for(let i=0;i<10;i++){
            arrStudent.push(i);
        }

        itemSpecify=arrStudent.filter( item=> item === 6);
        console.log('Danh sách 6 phần tử!');
        return itemSpecify;
    };

    const [dataT1,setDataT1] = useState('1');
    useEffect(()=>{
        setDataT1(dataT1+1);
    },[]);


    return (
        <div>
            <span style={{color:'#fff'}}>{dataT1}</span>
            <button onClick={() => Crease()}>Tăng</button>
        </div>
    );
}

export default Test_useMemo;