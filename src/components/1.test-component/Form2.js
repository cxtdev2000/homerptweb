
import React,{useState} from 'react';

export default function Form2()
{
    const [stepOpenAccount,setStepOpenAccount] = useState(0);

    return (
        <>
            {
                stepOpenAccount===0?(
                    <>
                        <label>
                            Step 1:
                            <input
                            name="password"
                            type="password"/>
                        </label>
                    </>
                ):stepOpenAccount===1?(
                    <>
                        <label>
                            Step 2:
                            <input
                            name="password"
                            type="password"/>
                        </label>
                    </>
                ):stepOpenAccount===2?(
                    <>
                        <label>
                            Step 3:
                            <input
                            name="password"
                            type="password"/>
                        </label>
                    </>
                ):(<>Finish</>)
            }
            <button onClick={()=>setStepOpenAccount(stepOpenAccount+1)}>Next Step</button>
        </>
    );
}