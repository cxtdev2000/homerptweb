import React,{useState} from "react";
import Form1 from './components/1.test-component/Form1';
import Form2 from './components/1.test-component/Form2';
import Form3 from './components/1.test-component/Form3';

function NextPage(){
    const [count,setCount]=useState(1);

    const handleClick = () => {
        setCount(count+1);    
        console.log(count);
    }

    let getForm;
    if(count===1)
    getForm=<Form1/>;
    else if(count===2)
    getForm=<Form2/>;
    else if(count===3)
    getForm=<Form3/>;

    return (
        <div>
            {
                count===1?(
                    <>
                    Thông tin 1
                    </>
                ):
                count===2?(
                    <>
                    show 2
                    </>
                ):
                count===3?(
                    <>
                    show 3
                    </>
                ):(
                    <></>
                )
            }
            <button onClick={handleClick}>Test</button>
        </div>
    );

}

export default NextPage;