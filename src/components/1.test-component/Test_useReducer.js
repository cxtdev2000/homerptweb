import React,{useReducer} from 'react';

// Cách sử dụng useReducer tương tự như useState
// Nhưng useReducer sử dụng để làm cái công việc phức tạp hơn so với useState
// ví dụ như:
// Nếu bạn cần lấy các useState phụ thuộc nhau để làm 1 việc gì đó
// Gồm 4 bước
/*
    1. Init State
    2. Actions
    3. Reducer
    4. Dispath
*/

// 1. Init State 
const initState=0; // state ở đây có thể là 1 kiểu dữ liệu bất kỳ

// 2. Actions
 const UP_ACTION='up';
 const DOWN_ACTION='down';

 // 3. Reducer
const reducer = (state,action) =>{
    console.log('reducer running...');
    switch(action){
        case UP_ACTION:
            return state+1;
        case DOWN_ACTION:
            return state-1;
        default:
            throw new Error('Invalid action');
    }
}
// 4. Dispath 
function Test_useReducer() {
    const [count,dispath] = useReducer(reducer,initState);
    return (
        <div>
           <h1>{count}</h1>
           <button onClick={()=> dispath(UP_ACTION)}>Up</button> 
           <button onClick={()=>dispath(DOWN_ACTION)}>Down</button>
        </div>
    );
}

export default Test_useReducer;