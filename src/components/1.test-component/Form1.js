import React,{useState,useEffect,useRef} from "react";
import { apiGetListAllCode } from '../../helpers/apiAllcodeHelper';

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";


export default function Form1()
{
    const [objData, setState] = useState({
        username: '',
        password: '',
        field3:'',
        fieldDate:new Date()
    });

    const printValues = e => {
        e.preventDefault();
        //console.log(form.username, form.password,form.field3);
    };
      //
    const updateField = e => {
        setState({
            ...objData,
            [e.target.name]: e.target.value
        });
    };

    const [dataUserInfo, setDataUserInfo] = useState({});
    useEffect(()=>{
        apiGetListAllCode().then((res) => {
            setDataUserInfo(res.data);
        });
    },[])


    const handleSelectChange = (e) => {
        setState({
            ...objData,
            field3: e.target.value
        });
    };

    // const [toDate, setToDate] = useState('');
   
    const handleDateChange = (date) => {
        setState({
            ...objData,
            fieldDate: date
        });
    };
    
    console.log(objData);
    
    // const handleClick = () => {
    //     setState({
    //         ...objData,
    //         fieldDate: toDate
    //     });
    //     console.log(objData);
    // };
    const [gender, setGender] = useState('male');

    const handleChange = (event) => {
      setGender(event.target.value)
    }
    console.log('you select :',gender);

    const textInput = useRef();
    const focusTextInput = () => textInput.current.focus();
    return (
        <div style={{color:'#fff'}}>
            {/* {JSON.stringify(dataUserInfo)} */}
            <form onSubmit={printValues}>
                <label>
                    Field 1:
                    <input
                    value={objData.username}
                    name="username"
                    onChange={updateField}/>
                </label>
                <br />
                <label>
                    Field 2:
                    <input
                    value={objData.password}
                    name="password"
                    type="password"
                    onChange={updateField}/>
                </label>
                <br/>
                <label>
                    Field 3:
                    <select onChange={handleSelectChange}>
                        <option value=""></option>
                        <option value="Apple">Apple</option>
                        <option value="Orange">Orange</option>
                        <option value="Pineapple">Pineapple</option>
                        <option value="Banana">Banana</option>
                    </select>
                </label>
                <br/>
                <label>
                    Field 4:
                <DatePicker dateFormat="dd/MM/yyyy" 
                    selected={objData.fieldDate}
                    onChange={(value)=>handleDateChange(value)} />
                </label>
                <br />
                <p>Gender</p>
                <div>
                    <input
                    type="radio"
                    value="male"
                    checked={gender === 'male'}
                    onChange={handleChange}
                    /> Male
                </div>
                <div>
                    <input
                    type="radio"
                    value="female"
                    checked={gender === 'female'}
                    onChange={handleChange}
                    /> Female
                </div>
                <button>Submit</button>
                <br />
                <input type="text" ref={textInput}/>
                <button onClick={focusTextInput}>using Ref</button>
            </form>
            {/* <button onClick={handleClick}>Test</button> */}
        </div>
    );
}