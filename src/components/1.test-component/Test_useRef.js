import React,{useRef, useState} from "react";

//Các trường hợp phổ biến khi cần phải truy cập tới các thành phần con trong DOM 
// đó là làm việc với input:
function Test_useRef() {
    const refContainer=useRef("");
    const [valueInput,setValueInput]=useState('nhap gia tri vao day');
    //
    const handleButtonClick=()=> {

    };

    const handleChangeInput = (e) =>{
        setValueInput(e.target.value);
    }

    return (
        <>
            <input ref={refContainer} value={valueInput} onChange={handleChangeInput} type="text"/>
            <button onClick={handleButtonClick}>Focus the input</button>
        </>
    );
}

export default Test_useRef;
