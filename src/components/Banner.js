import { useTranslation } from "react-i18next";

export default function Banner() {
  const { t } = useTranslation();
  return (
    <>
      <section className="section__banner">
        <div className="container">
          <div className="banner">
            <div className="banner__content">
              <h3 className="heading-primary">{t("VietinBankCapital")}</h3>
              <span className="paragraph">{t("ketNoiGiaoDichTrucTuyen")}</span>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
