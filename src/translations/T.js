import i18n from "i18next";
import { changeLanguage } from "../redux/actions/sharedActions";

export const t = (str) => {
  return i18n.t(str);
};

const T = ({ text }) => {
  return t(text);
};

export const hanldeChangeLanguage = (lng, dispatch) => {
  console.log("changing language: ", lng)
  i18n.changeLanguage(lng);
  changeLanguage(dispatch, lng);
};

export default T;
