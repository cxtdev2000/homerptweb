import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import langVi from "./vi.json";
import langEn from "./en.json";

import { getLanguageFromStorage } from "../redux/actions/sharedActions";
const c_initLanguage = getLanguageFromStorage();

const resources = {
  vi: {
    translations: langVi,
  },
  en: {
    translations: langEn,
  },
};

i18n.use(initReactI18next).init({
  resources,
  lng: c_initLanguage && c_initLanguage !== "" ? c_initLanguage : "vi", // default language
  fallbackLng: "vi", // when specified language translations not present
  ns: ["translations"],
  defaultNS: "translations",
  keySeparator: false,
  interpolation: {
    escapeValue: false,
    formatSeparator: ",",
  },
  react: {
    // bỏ thuộc tính wait này để loại bỏ warning It seems you are still using the old wait option, you may migrate to the new useSuspense behaviour.
    // wait: true,
  },
});
export default i18n;

//
export const transByLanguage = (string, lang) => {
  if (resources[lang]) {
    return resources[lang].translations[string] ?? string;
  } else {
    return langVi[string] ?? string;
  }
};
